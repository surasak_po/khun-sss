
let editId = ""
let searchBar = "TODAY";

$(document).ready(function () {
    $("#addModal").modal('hide');
    $("#editModal").modal('hide');
    init();
  
});

async function init(){
    await initData();
}
function clickLoadStatus(status){
    searchBar = status
    initData();
}

function splitMoney(barberId){
    fetch("./api/moneymanagement.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "UPDATE_MONEY",
            barber_id:barberId
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
                showToast('เบิกเงินสำเร็จ','success');
           }else{
            if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });

    initData();

}
async function initData(){
    fetch("./api/moneymanagement.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "MONEY_DETAIL"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                let totalPrice = 0
                let splitPrice = 0
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    if(item['types']=="total"){
                        totalPrice =item['sum_price'];
                        $("#total_price").html(item['sum_price']);
                    }
                    if(item['types']=="is_split"){
                        splitPrice =item['sum_price'];
                        $("#split_price").html((item['sum_price']/2)*100/100);
                    }
                    if(item['types']=="is_not_split"){
                        $("#unsplit_price").html((item['sum_price']/2)*100/100);
                    }
                }
                // $("#net_price").html(totalPrice-splitPrice);
            }else{
               
            }
           }
    });

    $("#table_content").html("");
    fetch("./api/moneymanagement.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "USER_LIST"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                for(let i=0;i<response['datas'].length;i++){
                    let totalPrice = 0
                    let splitPrice = 0
                    let unSplitPrice = 0
                    const item = response['datas'][i];
                    if(item['monney_detail']){
                        if(item['monney_detail'].length>0){
                            for(let i=0;i<item['monney_detail'].length;i++){
                                const itemData = item['monney_detail'][i];
                                if(itemData['types']=="total"){
                                    totalPrice =itemData['sum_price'];
                                }
                                if(itemData['types']=="is_split"){
                                    splitPrice =itemData['sum_price'];
                                }
                                if(itemData['types']=="is_not_split"){
                                    unSplitPrice =itemData['sum_price'];
                                }
                            }
                        }
                    }
                    btnTemplate = '<td><center><a  class="btn btn-secondary" >แบ่งเงิน</a></center></td>  ';

                    if(unSplitPrice>0){
                        btnTemplate = `<td><center><a  class="btn btn-info" onclick="splitMoney('${item['barber_id']}')">แบ่งเงิน</a></center></td>`;
                    }
                    $("#table_content").append(`
                    <tr class="w3-border">
                        <td><center>${i+1}</center></td>
                        <td><center>${item['names']}</center></td>    
                        <td><center><img class="img" src="upload/${item['images']}"  alt=""></center></td>                        
                        <td><center>${(totalPrice/2)*100/100}</center></td>  
                        <td><center>${(splitPrice/2)*100/100}</center></td>    
                        <td><center>${(unSplitPrice/2)*100/100}</center></td>    
                        ${btnTemplate}             
                    </tr>
                    `);
                }
            }else{
                $("#table_content").append(`
                <tr class="w3-border">
                    <td colspan="7" class="text-center">ไม่มีข้อมูล</td>
                </tr>
                `);
            }
           }
    });
}

function closeAddModal(){
    $("#viewModal").modal('hide');
}


function generateBtnStatus(item){
    if(searchBar != "TODAY"){
        return `<span >-</span>`;
    }
    if(item.book_status=="APPROVE"){
        return `<span  class="btn btn-success" onclick="clickapprove(${item.book_ids})">ยืนยันทำรายการ</span>`;
    }
    return '';
}

function clickapprove(id){
    fetch("./api/approvebooking.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "APPROVE_BOOK",
            book_id:id
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
                showToast('อัพเดทข้อมูลสำเร็จ','success');
                initData();
           }
    });
}

function closeAddModal(){
    $("#viewModal").modal('hide');
}


function showToast(message,status){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: status,
        title: message
      })
   
}