

$(document).ready(function () {
    init();
});

async function init(){
    await initData();
    initEvent();
}

async function initData(){
    $("#table_content").html("");
    fetch("./api/user.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    $("#table_content").append(`
                    <tr class="w3-border">
                        <td><center>${item['username']}</center></td>
                        <td><center>${item['email']}</center></td>
                        <td><center>${item['userlevel']}</center></td>
                        <td><center><a onclick="deleteData(${item['id']})" class="btn btn-danger" >ลบ</a></center></td>                        
                    </tr>
                    `);
                }
            }else{
                $("#table_content").append(`
                <tr class="w3-border">
                    <td colspan="4" class="text-center">ไม่มีข้อมูล</td>
                </tr>
                `);
            }
           }
    });
}

