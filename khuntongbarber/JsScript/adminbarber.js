
let imageBase64 = ""
let imageBase64Edit = ""
let flagDeleteImageEdit = false;
let barberId = ""

$(document).ready(function () {
    $("#addBarber").modal('hide');
    $("#editBarber").modal('hide');
    init();
});

async function init(){
    await initData();
    initEvent();
}

async function initData(){
    $("#table_content").html("");
    fetch("./api/barber.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    $("#table_content").append(`
                    <tr class="w3-border">
                        <td><center>${item['names']}</center></td>
                        <td><center>${item['age']}</center></td>    
                        <td><center>${item['phone']}</center></td>    
                        <td><center>${item['email']}</center></td>                             
                        <td><center><img class="img" src="upload/${item['images']}"  alt=""></center></td>                        
                        <td><center><a onclick="editbarber(${item['barber_id']})" class="btn btn-warning">แก้ไข</a></center></td>                        
                        <td><center><a onclick="deletebarber(${item['barber_id']})" class="btn btn-danger" >ลบ</a></center></td>                        
                    </tr>
                    `);
                }
            }else{
                $("#table_content").append(`
                <tr class="w3-border">
                    <td colspan="7" class="text-center">ไม่มีข้อมูล</td>
                </tr>
                `);
            }
           }
    });
}


function closeAddModal(){
    $("#addBarber").modal('hide');
}
function closeeditModal(){
    $("#editBarber").modal('hide');
}

function UploadFile(file){
    var input = file;
    var url = $(file).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
     {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#showImageAdd").removeClass("d-none");
            $("#uploadAdd").addClass("d-none");
            $('#imageAdd').attr('src', e.target.result);
            imageBase64 = e.target.result;
        }
       reader.readAsDataURL(input.files[0]);
    }
    else
    {
    //   $('#img').attr('src', '/assets/no_preview.png');
    }
}

function initEvent(){
    $("#btn_addBarber").click(function(){
        $("#showImageAdd").addClass("d-none");
        $("#uploadAdd").removeClass("d-none");
        $('#imageAdd').attr('src', "");
        $("#avatarAdd").val(null);
        imageBase64 = ""
        $("#addBarber").modal('show');
    });
    
}

function deleteAddImage(){
    $("#showImageAdd").addClass("d-none");
    $("#uploadAdd").removeClass("d-none");
    $('#imageAdd').attr('src', "");
    $("#avatarAdd").val(null);
    imageBase64 = ""
}

function deleteEditImage(){
    $("#showImageEdit").addClass("d-none");
    $("#uploadEdit").removeClass("d-none");
    $('#imageEdit').attr('src', "");
    $("#avatarEdit").val(null);
    imageBase64 = ""
    flagDeleteImageEdit = true;
}

function UploadFileEdit(file){
    var input = file;
    var url = $(file).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
     {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#showImageEdit").removeClass("d-none");
            $("#uploadEdit").addClass("d-none");
            $('#imageEdit').attr('src', e.target.result);
            imageBase64Edit = e.target.result;
        }
       reader.readAsDataURL(input.files[0]);
    }
    else
    {
    //   $('#img').attr('src', '/assets/no_preview.png');
    }
}

function editbarber(id){
    barberId = id;
    fetch("./api/barber.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET",
            barber_id:id,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                const item = response['datas'][0];
                $("#txt_name_edit").val(item['names']);
                $("#txt_age_edit").val(item['age']);
                $("#txt_mobile_edit").val(item['phone']);
                $("#txt_email_edit").val(item['email']);
                $('#imageEdit').attr('src', "upload/"+item['images']);
                $("#editBarber").modal('show');
                $("#showImageEdit").removeClass("d-none");
                $("#uploadEdit").addClass("d-none");
                $("#avatarEdit").val(null);
            }else{
              
            }
           }
    });
}

function deletebarber(Id){
    fetch("./api/barber.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "DELETE",
            barber_id:Id,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            showToast('ลบข้อมูลสำเร็จ','success');
            initData();
           }else{
               if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else if(response['message']==="USED"){
                showToast('ไม่สามารถลบข้อมูลได้','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });
}

function addData(){
    fetch("./api/barber.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "POST",
            names:$("#txt_name").val(),
            age:$("#txt_age").val(),
            phone:$("#txt_mobile").val(),
            email:$("#txt_email").val(),
            images:imageBase64,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            showToast('เพิ่มข้อมูลสำเร็จ','success');
            $("#addBarber").modal('hide');
            initData();
           }else{
               if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });
}


function updateData(){
    fetch("./api/barber.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "PUT",
            barber_id:barberId,
            names:$("#txt_name_edit").val(),
            age:$("#txt_age_edit").val(),
            phone:$("#txt_mobile_edit").val(),
            email:$("#txt_email_edit").val(),
            images:imageBase64Edit,
            flag:flagDeleteImageEdit,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            showToast('บันทึกข้อมูลสำเร็จ','success');
            $("#editBarber").modal('hide');
            initData();
           }else{
               if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });
}


function showToast(message,status){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: status,
        title: message
      })
   
}