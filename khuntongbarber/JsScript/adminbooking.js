
let imageBase64 = ""
let imageBase64Edit = ""
let flagDeleteImageEdit = false;
let editId = ""

$(document).ready(function () {
    $("#addModal").modal('hide');
    $("#editModal").modal('hide');
    init();

  
});

async function init(){
    await initData();
    initEvent();
}

async function initData(){
    $("#table_content").html("");
    fetch("./api/book.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GETLIST"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    $("#table_content").append(`
                    <tr class="w3-border">
                        <td><center>${item['services_name']}</center></td>
                        <td><center>${item['services_price']}</center></td>
                        <td><center>${item['username']}</center></td>
                        <td><center>${item['barber_name']}</center></td>
                        <td><center>${item['create_time']}</center></td>
                        <td><center>${item['date_time'].split(" ")[0]}</center></td>
                        <td><center>${item['book_time']}</center></td>
                        <td><center>${showBookStatus(item['book_status'])}</center></td>
                        <td><center>${generateBtnStatus(item)}</center></td>                        
                    </tr>
                    `);
                }
            }else{
                $("#table_content").append(`
                <tr class="w3-border">
                    <td colspan="4" class="text-center">ไม่มีข้อมูล</td>
                </tr>
                `);
            }
           }
    });
}

function showBookStatus(status){
    if(status=="WAIT_APPROVE"){
        return `<span class="badge bg-warning">รออนุมัติ</span>`;
    }
    if(status=="APPROVE"){
        return `<span class="badge bg-success">อนุมัติแล้ว</span>`;
    }
    if(status=="REJECT"){
        return `<span class="badge bg-danger">ปฏิเสธ</span>`;
    }
    return '';
}

function generateBtnStatus(item){
    if(item.book_status=="WAIT_APPROVE"){
        return `<span onclick="viewData(${item.book_id})" class="btn btn-info">ตรวจสอบ</span>`;
    }
    if(item.book_status=="APPROVE"){
        return `<span  class="text-white">ยืนยันแล้ว</span>`;
    }
    if(item.book_status=="REJECT"){
        return `<span  class="text-white">ปฏิเสธแล้ว</span>`;

    }
    return '';
}

function closeAddModal(){
    $("#viewModal").modal('hide');
}


function UploadFile(file){
    var input = file;
    var url = $(file).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
     {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#showImageAdd").removeClass("d-none");
            $("#uploadAdd").addClass("d-none");
            $('#imageAdd').attr('src', e.target.result);
            imageBase64 = e.target.result;
        }
       reader.readAsDataURL(input.files[0]);
    }
    else
    {
    //   $('#img').attr('src', '/assets/no_preview.png');
    }
}


function initEvent(){
    $("#btn_addBarber").click(function(){
        $("#showImageAdd").addClass("d-none");
        $("#uploadAdd").removeClass("d-none");
        $('#imageAdd').attr('src', "");
        $("#avatarAdd").val(null);
        imageBase64 = ""
        $("#addModal").modal('show');
    });
    
}


function deleteAddImage(){
    $("#showImageAdd").addClass("d-none");
    $("#uploadAdd").removeClass("d-none");
    $('#imageAdd').attr('src', "");
    $("#avatarAdd").val(null);
    imageBase64 = ""
}

function deleteEditImage(){
    $("#showImageEdit").addClass("d-none");
    $("#uploadEdit").removeClass("d-none");
    $('#imageEdit').attr('src', "");
    $("#avatarEdit").val(null);
    imageBase64 = ""
    flagDeleteImageEdit = true;
}

function UploadFileEdit(file){
    var input = file;
    var url = $(file).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
     {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#showImageEdit").removeClass("d-none");
            $("#uploadEdit").addClass("d-none");
            $('#imageEdit').attr('src', e.target.result);
            imageBase64Edit = e.target.result;
        }
       reader.readAsDataURL(input.files[0]);
    }
    else
    {
    }
}

function viewData(id){
    console.log(id);
    editId = id;
    fetch("./api/book.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET",
            book_id:id,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                const item = response['datas'][0];
                console.log(item);
                $("#image_book_content").attr("src", "upload/"+item['images']);
                $("#viewModal").modal('show');
            }else{
              
            }
           }
    });
}

function deleteData(Id){
    fetch("./api/services.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "DELETE",
            services_id:Id,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            showToast('ลบข้อมูลสำเร็จ','success');
            initData();
           }else{
               if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });
}
function updateBookingStatus(status){
    fetch("./api/book.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "UPDATE_STAUTS",
            status:status,
            book_id:editId,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            showToast('อนุมัติ / ปฏิเสธ สำเร็จ','success');
            $("#viewModal").modal('hide');
            initData();
           }else{
               if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });
}


function showToast(message,status){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: status,
        title: message
      })
   
}