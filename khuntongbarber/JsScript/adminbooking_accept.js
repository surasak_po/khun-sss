
let editId = ""
let searchBar = "TODAY";

$(document).ready(function () {
    $("#addModal").modal('hide');
    $("#editModal").modal('hide');
    init();
  
});

async function init(){
    await initData();
}
function clickLoadStatus(status){
    searchBar = status
    initData();
}


async function initData(){
    $("#table_content").html("");
    fetch("./api/approvebooking.php", {
        method: "POST",
        body: JSON.stringify({
            mode: searchBar
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    $("#table_content").append(`
                    <tr class="w3-border">
                        <td><center>${item['services_name']}</center></td>
                        <td><center>${item['services_price']}</center></td>
                        <td><center>${item['username']}</center></td>
                        <td><center>${item['barber_name']}</center></td>
                        <td><center>${item['booking_times']}</center></td>
                        <td><center>${item['date_time'].split(" ")[0]}</center></td>
                        <td><center>${item['book_time']}</center></td>
                        <td><center>${generateBtnStatus(item)}</center></td>                        
                    </tr>
                    `);
                }
            }else{
                $("#table_content").append(`
                <tr class="w3-border">
                    <td colspan="9" class="text-center">ไม่มีข้อมูล</td>
                </tr>
                `);
            }
           }
    });
}

function closeAddModal(){
    $("#viewModal").modal('hide');
}


function generateBtnStatus(item){
    if(searchBar != "TODAY"){
        return `<span >-</span>`;
    }
    if(item.book_status=="APPROVE"){
        return `<span  class="btn btn-success" onclick="clickapprove(${item.book_ids})">ยืนยันทำรายการ</span>`;
    }
    return '';
}

function clickapprove(id){
    fetch("./api/approvebooking.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "APPROVE_BOOK",
            book_id:id
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
                showToast('อัพเดทข้อมูลสำเร็จ','success');
                initData();
           }
    });
}

function closeAddModal(){
    $("#viewModal").modal('hide');
}


function showToast(message,status){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: status,
        title: message
      })
   
}