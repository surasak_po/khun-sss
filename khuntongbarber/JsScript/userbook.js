let today = new Date();
let currentMonth = today.getMonth();
let currentYear = today.getFullYear();
let selectYear = document.getElementById("year");
let selectMonth = document.getElementById("month");
let months = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
let monthAndYear = document.getElementById("monthAndYear");
let dateFilter = ""
// init
let elementList = [];
let isInitElementList = false;
let isShowBtnSave = true;
// IMAGE
let imageBase64 = ""
let imageBase64Edit = ""
let flagDeleteImageEdit = false;
// IMAGE
if(!isInitElementList){
    elementList=[];
    isInitElementList = false;
    showCalendar(currentMonth, currentYear);
    setTimeout(function(){
        initClickEvent();
    },2000)
}

function initClickEvent() {
    if(!isInitElementList){
        for(let i = 0;i<elementList.length;i++){
            $("#"+elementList[i]).click(function(){
                initBarberList();
                clearCheckBox();
                initServiceList();
                $("#sel_barber").val("");
                $("#sel_service").prop('disabled', false);
                $("#btn_save_time").hide();
                $("#sel_barber").prop('disabled', true);
                const itemSplit = elementList[i].split("-")
                let day = itemSplit[1] > 9 ? itemSplit[1] : "0"+itemSplit[1];
                let month = itemSplit[2] > 9 ? itemSplit[2] : "0"+itemSplit[2];
                dateFilter = itemSplit[3]+"-"+ month+"-"+ day;

                // CALDATE
                const dateSelected = new Date(dateFilter)
                const currentDate = new Date()
                const oneDay = 1000 * 60 * 60 * 24;
                const diff = dateSelected.getTime() - currentDate.getTime();
                const diffInDays = Math.round(diff / oneDay);
                for(let i = 0;i<window.MapCheckDate.length;i++){
                    const item = window.MapCheckDate[i];
                    $("#"+item).prop('disabled', true);
                    $("#"+item+"_block").css( "background-color", "white" );
                    $("#"+item+"_block").css( "color", "#A7A9AB" );
                }
                if(diffInDays < 0){
                    $("#sel_service").prop('disabled', true);
                    isShowBtnSave = false;
                }else{
                    isShowBtnSave = true;
                }
                // END

                $("#txt_mng_date").html(`วันที่ ${day} เดือน ${month} ปี ${itemSplit[3]}`)
                $("#modal_mng_times").modal('show');
            });
        }
    }
}

function next() {
    elementList=[];
    isInitElementList = false;
    currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
    currentMonth = (currentMonth + 1) % 12;
    showCalendar(currentMonth, currentYear);
    setTimeout(function(){
        initClickEvent();
    },2000)
}

function previous() {
    elementList=[];
    isInitElementList = false;
    currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
    currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
    showCalendar(currentMonth, currentYear);
    setTimeout(function(){
        initClickEvent();
    },2000)

}

function showCalendar(month, year) {
    let firstDay = (new Date(year, month)).getDay();
    let daysInMonth = 32 - new Date(year, month, 32).getDate();
    let tbl = document.getElementById("calendar-body"); 
    // clearing all previous cells
    tbl.innerHTML = "";
    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;
    selectYear.value = year;
    selectMonth.value = month;
    // creating all cells
    let date = 1;

    for (let i = 0; i < 6; i++) {
        // creates a table row
        let row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (let j = 0; j < 7; j++) {
            if (i === 0 && j < firstDay) {
                let cell = document.createElement("td");
                let cellText = document.createTextNode("");
                cell.classList.add("disabled-date"); //หลัง
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            else if (date > daysInMonth) {
                break;
            }else {

                let cell = document.createElement("td");
                let cellText = document.createTextNode(date);
                if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
                    cell.classList.add("current-date"); //วันปัจจุบัน
                    cell.classList.add("cursor-pointer");
                } else{
                    if(year === today.getFullYear() && month === today.getMonth()){
                        if(date < today.getDate() ){
                            cell.classList.add("disabled-date"); //หลัง
                        }else{
                            cell.classList.add("cursor-pointer");
                            cell.classList.add("enabled-date");
                        }
                    }else{
                        if ( month < today.getMonth() && year == today.getFullYear()){
                            cell.classList.add("disabled-date"); //วันปัจจุบัน
                        }
                        else if ( year < today.getFullYear() ) {
                            cell.classList.add("disabled-date"); //วันปัจจุบัน
                        }else{
                            cell.classList.add("cursor-pointer");
                            cell.classList.add("enabled-date");
                        }
                    }
                }
               
                let elementId = "day-"+date+"-"+Number(month+1)+"-"+year;
                elementList.push(elementId);
                cell.setAttribute("id", elementId );
                cell.appendChild(cellText);
                row.appendChild(cell);
                date++;
            }
        }
        tbl.appendChild(row);
    }

}

function initBarberList(){
    $("#sel_barber").html(`<option value="" disabled hidden selected>เลือกช่าง</option>`)
    fetch("http://127.0.0.1/khuntongbarber/api/barber.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                $("#sel_barber").val("");
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    $("#sel_barber").append(`<option value="${item['barber_id']}">${item['names']}</option>`)
                }

                
            }else{
               
            }
           }
    });
}


function booking(){
    fetch("http://127.0.0.1/khuntongbarber/api/book.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "POST",
            images:imageBase64,
            users_id:window.userId,
            services_id:$("#sel_service").val(),
            time_id:$("input[name='rad_times']:checked").val(),
            book_time:$("input[name='rad_times']:checked").parent('label').text(),
            book_status:"WAIT_APPROVE",
            deposit_price:50,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            showToast('จองสำเร็จ','success');
            $("#modal_book_image").modal('hide');
            setTimeout(function() {
                location.reload();
            }, 2000);
           }else{
               if(response['message']==="REQUIRE"){
                showToast('กรุณากรอกข้อมูลให้ครบถ้วน','warning');
               }else{
                showToast('เกิดข้อผิดพลาด','error');
               }
           }
    });
}

function initServiceList(){
    $("#sel_service").html(`<option value="" disabled hidden selected>เลือกบริการ</option>`)
    fetch("http://127.0.0.1/khuntongbarber/api/services.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET"
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
            if(response['datas'].length>0){
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    $("#sel_service").append(`<option value="${item['services_id']}">${item['names']} ราคา : ${item['price']} บาท เวลา : ${item['duration']} นาที</option>`)
                }
            }else{
               
            }
           }
    });
}

function clearCheckBox(){
    for(let i = 0;i<window.MapCheckDate.length;i++){
        const item = window.MapCheckDate[i];
            $("#"+item).prop('checked', false);
            $("#"+item+"_block").css( "background-color", "white" );
    }
}

window.MapDateTime = {
    "11.00" : "check_11",
    "13.00" : "check_13",
    "14.00" : "check_14",
    "15.00" : "check_15",
    "16.00" : "check_16",
    "17.00" : "check_17",
    "18.00" : "check_18",
    "19.00" : "check_19",
    "20.00" : "check_20",
    "21.00" : "check_21",
}

window.MapCheckDate = [
    "check_11",
    "check_13",
    "check_14",
    "check_15",
    "check_16",
    "check_17",
    "check_18",
    "check_19",
    "check_20",
    "check_21",
]

window.CheckToTime = {
    "check_11" : "11.00",
    "check_13" : "13.00",
    "check_14" : "14.00",
    "check_15" : "15.00",
    "check_16" : "16.00",
    "check_17" : "17.00",
    "check_18" : "18.00",
    "check_19" : "19.00",
    "check_20" : "20.00",
    "check_21" : "21.00",
}


$("#sel_service").change(function(){
    $("#sel_barber").prop('disabled', false);
});

$("#sel_barber").change(function(){
    clearCheckBox();
    const barberId = $("#sel_barber").val();
    fetch("http://127.0.0.1/khuntongbarber/api/times_manage_ment.php", {
        method: "POST",
        body: JSON.stringify({
            mode: "GET_BY_BARBER",
            barber_id:barberId,
            date:dateFilter,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then(function (response) {
           if(response['message']==="SUCCESS"){
               if(isShowBtnSave){
                for(let i = 0;i<window.MapCheckDate.length;i++){
                    const item = window.MapCheckDate[i];
                        $("#"+item).prop('disabled', false);
                        // $("#"+item+"_block").css( "color", "black" );
                        $("#"+item+"_block").css( "color", "#A7A9AB" );

                }
               }
            if(response['datas'].length>0){
                for(let i = 0;i<window.MapCheckDate.length;i++){
                    const item = window.MapCheckDate[i];
                    if(!$("#"+item).is(":checked")){
                        $("#"+item).prop('disabled', true);
                        $("#"+item+"_block").css( "background-color", "white" );
                        // $("#"+item+"_block").css( "color", "#A7A9AB" );
                        $("#"+item+"_block").css( "color", "black" );

                    }
                }
                for(let i=0;i<response['datas'].length;i++){
                    const item = response['datas'][i];
                    if(item['book_status'] != null){
                        let found = window.MapDateTime[item['time_str']];
                        if(found){
                            if(item['book_status']=="REJECT"){
                                $("#"+found).val(item['time_id'])
                                $("#"+found).prop('disabled', false);
                            }else{
                                if(item['book_status']=="APPROVE"){
                                    $("#"+found+"_block").css( "background-color", "#b9feb9" );
                                    $("#"+found).prop('disabled', true);
                                }
                                if(item['book_status']=="WAIT_APPROVE"){
                                    $("#"+found+"_block").css( "background-color", "#ffc107" );
                                    $("#"+found).prop('disabled', true);
                                }
                            }
                        }
                    }else{
                        let found = window.MapDateTime[item['time_str']];
                        if(found){
                            $("#"+found).val(item['time_id'])
                            $("#"+found).prop('disabled', false);
                        }

                    }
                }
               
            }else{
                for(let i = 0;i<window.MapCheckDate.length;i++){
                    const item = window.MapCheckDate[i];
                    if(!$("#"+item).is(":checked")){
                        $("#"+item).prop('disabled', true);
                        $("#"+item).val(item)

                    }
                }
            }
           }
    });
})

$("#btn_save_time").click(function(){
    // const barberId = $("#sel_barber").val();
    
    $("#modal_book_image").modal('show');

});


$('input[type=radio][name=rad_times]').change(function() {
    console.log(this.value);
    if(this.value != ""){
        $("#btn_save_time").show();
        $("#lbl_service").html($("#sel_service option:selected").text());
        $("#lbl_barber").html($("#sel_barber option:selected").text());
        $("#lbl_book").html($("input[name='rad_times']:checked").parent('label').text());

        
    }
});

function UploadFile(file){
    var input = file;
    var url = $(file).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
     {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#showImageAdd").removeClass("d-none");
            $("#uploadAdd").addClass("d-none");
            $('#imageAdd').attr('src', e.target.result);
            imageBase64 = e.target.result;
        }
       reader.readAsDataURL(input.files[0]);
    }
    else
    {
    //   $('#img').attr('src', '/assets/no_preview.png');
    }
}

function deleteAddImage(){
    $("#showImageAdd").addClass("d-none");
    $("#uploadAdd").removeClass("d-none");
    $('#imageAdd').attr('src', "");
    $("#avatarAdd").val(null);
    imageBase64 = ""
}


