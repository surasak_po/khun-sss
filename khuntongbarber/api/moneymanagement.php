<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="MONEY_DETAIL"){
      
        $strSQL = "SELECT tb1.sum_price,'total' AS types FROM(
            SELECT CASE WHEN tb2.services_price IS NULL THEN 0 ELSE Sum(tb2.services_price)  END AS sum_price
            FROM tbl_approve_work tb1
            LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
            LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id
            ) tb1
        UNION ALL
        SELECT tb2.sum_price,'is_split' AS types FROM(
            SELECT CASE WHEN tb2.services_price IS NULL THEN 0 ELSE Sum(tb2.services_price)  END AS sum_price
            FROM tbl_approve_work tb1
            LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
            LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id 
            WHERE is_split_money = TRUE  
            ) tb2
        UNION ALL
        SELECT tb3.sum_price,'is_not_split' AS types FROM(
            SELECT CASE WHEN tb2.services_price IS NULL THEN 0 ELSE Sum(tb2.services_price)  END AS sum_price
            FROM tbl_approve_work tb1
            LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
            LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id 
            WHERE is_split_money = FALSE  
            ) tb3
        ";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;

    }

    if($mode=="USER_LIST"){
      
        $strSQL = "SELECT * FROM tbl_barber ";
        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $strMoneyDetailSQL = "SELECT tb1.sum_price,'total' AS types FROM(
                SELECT CASE WHEN tb2.services_price IS NULL THEN 0 ELSE Sum(tb2.services_price)  END AS sum_price
                FROM tbl_approve_work tb1
                LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
                LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id
                INNER JOIN  tbl_time tb4 ON tb2.time_id  = tb4.time_id 
                WHERE tb4.barber_id  = '".$r['barber_id']."'
                ) tb1
            UNION ALL
            SELECT tb2.sum_price,'is_split' AS types FROM(
                SELECT CASE WHEN tb2.services_price IS NULL THEN 0 ELSE Sum(tb2.services_price)  END AS sum_price
                FROM tbl_approve_work tb1
                LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
                LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id 
                INNER JOIN  tbl_time tb4 ON tb2.time_id  = tb4.time_id 
                WHERE is_split_money = TRUE  AND tb4.barber_id  = '".$r['barber_id']."'
                ) tb2
            UNION ALL
            SELECT tb3.sum_price,'is_not_split' AS types FROM(
                SELECT CASE WHEN tb2.services_price IS NULL THEN 0 ELSE Sum(tb2.services_price)  END AS sum_price
                FROM tbl_approve_work tb1
                LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
                LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id 
                INNER JOIN  tbl_time tb4 ON tb2.time_id  = tb4.time_id 
                WHERE is_split_money = FALSE  AND tb4.barber_id  = '".$r['barber_id']."'
                ) tb3
            ";
            $sth1 = mysqli_query($conn,$strMoneyDetailSQL);
            $rowcount=mysqli_num_rows($sth1);
            if($rowcount>0){
                $rows1 = array();
                while($r1 = mysqli_fetch_assoc($sth1)) {
                    $rows1[] = $r1;
                }
                $r['monney_detail'] = $rows1;

            }else{
                $r['monney_detail'] = NULL;
            }

            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;

    }

    
    if($mode=="UPDATE_MONEY"){
        
        @$barber_id = trim($json_data['barber_id']);

        if($barber_id==""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $strSQL = "UPDATE tbl_approve_work tb1
        LEFT JOIN tbl_book tb2 ON tb1.book_id  = tb2.book_id AND tb2.book_status ='APPROVE'
        LEFT JOIN tbl_services tb3 ON tb2.services_id  = tb3.services_id 
        INNER JOIN  tbl_time tb4 ON tb2.time_id  = tb4.time_id 
        SET is_split_money = true
        WHERE is_split_money = FALSE  AND tb4.barber_id  = '".$barber_id."' ";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

}
?>