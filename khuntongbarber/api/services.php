<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GET"){
        @$services_id  = trim($json_data['services_id']);

        $strSQL ="SELECT * FROM tbl_services ";
        if($services_id  != ""){
            $strSQL = $strSQL." WHERE services_id  = '".$services_id ."' ";
        }
        $strSQL = $strSQL." ORDER BY services_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="POST"){

        @$names = trim($json_data['names']);
        @$price= trim($json_data['price']);
        @$duration= trim($json_data['duration']);


        if($names =="" || $price =="" || $duration =="" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }


        $strSQL = "INSERT INTO tbl_services (names,price,duration) VALUES( '".$names."','".$price."','".$duration."' )";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="PUT"){
        
        @$services_id = trim($json_data['services_id']);
        @$names = trim($json_data['names']);
        @$price= trim($json_data['price']);
        @$duration= trim($json_data['duration']);

        if($names =="" || $price =="" || $duration =="" || $services_id =="" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $strSQL = "UPDATE tbl_services SET names='".$names."',price='".$price."',duration='".$duration."' WHERE services_id = '".$services_id."' ";

        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="DELETE"){
        
        @$services_id = trim($json_data['services_id']);
        
        if($services_id == "" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>NULL));
            return 0;
        }else{
            $sql = "DELETE FROM tbl_services WHERE services_id = '".$services_id."' ";
            
            if ($conn->query($sql)) {
                echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
                return 0;
            }else{
                echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
                return 0;
            }

        }
        
    }

}
?>