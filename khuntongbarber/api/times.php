<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GET"){
        @$time_id  = trim($json_data['time_id']);

        $strSQL ="SELECT * FROM tbl_time ";
        if($time_id  != ""){
            $strSQL = $strSQL." WHERE time_id  = '".$time_id ."' ";
        }
        $strSQL = $strSQL." ORDER BY time_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="POST"){

        @$month_years = trim($json_data['names']);
        @$day_name_en = trim($json_data['day_name_en']);   
        @$day_name_th = trim($json_data['day_name_th']);   
        @$day_name_number = trim($json_data['day_name_number']);
        @$barber_id = trim($json_data['barber_id']);     
        
        if($month_years =="" || $day_name_en =="" || $day_name_th ==""  || $day_name_number =="" || $barber_id ==""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $strSQL = "INSERT INTO tbl_time (month_years,day_name_en,day_name_th,day_name_number,barber_id) VALUES( '".$month_years."','".$day_name_en."','".$day_name_th."','".$day_name_number."','".$barber_id."' )";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="DELETE"){
        
        @$time_id = trim($json_data['time_id']);
        if($time_id == "" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>NULL));
            return 0;
        }else{
            $sql = "DELETE FROM tbl_time WHERE time_id = '".$time_id."' ";
            
            if ($conn->query($sql)) {
                echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
                return 0;
            }else{
                echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
                return 0;
            }

        }
        
    }

}
?>