<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GETLIST"){
        @$status  = trim($json_data['status']);

        $strSQL = "SELECT *,tb3.names AS services_name,tb5.names AS barber_name FROM tbl_book tb1
        INNER JOIN user tb2 ON tb1.users_id = tb2.id
        INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
        INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id 
        INNER JOIN tbl_barber tb5 ON tb4.barber_id  = tb5.barber_id 
        ";

        if($status  != ""){
            $strSQL = "SELECT *,tb3.names AS services_name,tb5.names AS barber_name FROM tbl_book tb1
            INNER JOIN user tb2 ON tb1.users_id = tb2.id
            INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
            INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id
            INNER JOIN tbl_barber tb5 ON tb4.barber_id  = tb5.barber_id   WHERE book_status  = '".$status ."' ";
        }
        $strSQL = $strSQL." ORDER BY book_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="GETLISTUSER"){
        @$user_id  = trim($json_data['user_id']);

        $strSQL = "SELECT *,tb3.names AS services_name,tb5.names AS barber_name FROM tbl_book tb1
        INNER JOIN user tb2 ON tb1.users_id = tb2.id
        INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
        INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id 
        INNER JOIN tbl_barber tb5 ON tb4.barber_id  = tb5.barber_id 
        WHERE tb1.users_id = '".$user_id."'
        ";

        $strSQL = $strSQL." ORDER BY book_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="UPDATE_STAUTS"){
        //รับว่า ปฏิเสธ หรือ ยืนยัน
        @$status = trim($json_data['status']);
        @$book_id  = trim($json_data['book_id']);

        if($status=="" || $book_id==""  ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $strSQL = "UPDATE tbl_book SET book_status = '".$status."' WHERE book_id = '".$book_id."' ";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="GET"){
        
        @$book_id  = trim($json_data['book_id']);

        if($book_id==""   ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }
        $strSQL = "";
        if($book_id  != ""){
            $strSQL = $strSQL."SELECT * FROM tbl_book tb1
            INNER JOIN user tb2 ON tb1.users_id = tb2.id
            INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
            INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id  WHERE book_id  = '".$book_id ."' ";
        }

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="POST"){

        @$users_id = trim($json_data['users_id']); //
        @$services_id = trim($json_data['services_id']);   // 
        @$time_id = trim($json_data['time_id']);   //
        @$book_time = trim($json_data['book_time']); //
        @$book_status = trim($json_data['book_status']);   //
        @$deposit_price = trim($json_data['deposit_price']); //     
        @$images= trim($json_data['images']); //

        if($users_id =="" || $services_id =="" || $time_id ==""  || $book_time =="" || $book_status =="" || $deposit_price =="" || $images == ""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        if($images != ""){
            $imageSplit = explode(',',trim($json_data['images']));
            $file_type = explode("image/", $imageSplit[0]);
            $image_base64 = base64_decode($imageSplit[1]);
            $fileName = 'bookimage-'.rand().'.png';
            $file = UPLOAD_DIR . $fileName;
            @file_put_contents($file, $image_base64);
        }

        $strSQLGetService ="SELECT * FROM tbl_services WHERE services_id = '".$services_id."' ";
        $queryData = mysqli_query($conn,$strSQLGetService);
        $rowsData = array();
        while($r = mysqli_fetch_assoc($queryData)) {
            $rowsData[] = $r;
        }
        $servicesPrice = $rowsData[0]['price'];
        $servicesDuration = $rowsData[0]['duration'];
        $images = @$fileName;

        $strSQL = "INSERT INTO tbl_book (users_id,services_id,time_id,book_time,book_status,deposit_price,services_price,services_duration,images)
        VALUES( '".$users_id."','".$services_id."','".$time_id."','".$book_time."','".$book_status."','".$deposit_price."','".$servicesPrice."','".$servicesDuration."','".$images."' )";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }

    }

    if($mode=="DELETE"){
        
        @$book_id = trim($json_data['book_id']);
      
        if($book_id == "" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>NULL));
            return 0;
        }else{
            $sql = "DELETE FROM tbl_book WHERE book_id = '".$book_id."' ";
            if ($conn->query($sql)) {
                echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
                return 0;
            }else{
                echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
                return 0;
            }

        }
        
    }

}
?>