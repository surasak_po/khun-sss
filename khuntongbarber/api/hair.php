<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GET"){
        @$hair_id  = trim($json_data['hair_id']);

        $strSQL ="SELECT * FROM tbl_hair ";
        if($hair_id  != ""){
            $strSQL = $strSQL." WHERE hair_id  = '".$hair_id ."' ";
        }
        $strSQL = $strSQL." ORDER BY hair_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="POST"){

        @$names = trim($json_data['names']);
        @$images= trim($json_data['images']);

        if($names=="" || $images==""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $imageSplit = explode(',',trim($json_data['images']));
        $file_type = explode("image/", $imageSplit[0]);
        $image_base64 = base64_decode($imageSplit[1]);
        $fileName = 'hairimage-'.rand().'.png';
        $file = UPLOAD_DIR . $fileName;
        @file_put_contents($file, $image_base64);

        
        $strSQL = "INSERT INTO tbl_hair (names,images) VALUES( '".$names."','".$fileName."' )";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="PUT"){
        
        @$hair_id = trim($json_data['hair_id']);
        @$names = trim($json_data['names']);
        @$images= trim($json_data['images']);
        @$flag= trim($json_data['flag']);

        if($names=="" || $hair_id==""  ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        if($flag==TRUE){
            if($images == ""){
                echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
                return 0;
            }
        }

        $strSQL = "UPDATE tbl_hair SET names='".$names."' ";

        if($images != ""){
            $imageSplit = explode(',',trim($json_data['images']));
            $file_type = explode("image/", $imageSplit[0]);
            $image_base64 = base64_decode($imageSplit[1]);
            $fileName = 'hairimage-'.rand().'.png';
            $file = UPLOAD_DIR . $fileName;
            @file_put_contents($file, $image_base64);
            $strSQL = $strSQL.",images='".$fileName."' ";
        }

        $strSQL = $strSQL."WHERE hair_id = '".$hair_id."' ";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="DELETE"){
        
        @$hair_id = trim($json_data['hair_id']);
      
        if($hair_id == "" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>NULL));
            return 0;
        }else{
          
            $sql = "DELETE FROM tbl_hair WHERE hair_id = '".$hair_id."' ";
            if ($conn->query($sql)) {
                echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
                return 0;
            }else{
                echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
                return 0;
            }

        }
        
    }

}
?>