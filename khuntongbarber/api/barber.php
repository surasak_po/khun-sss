<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GET"){
        @$barber_id  = trim($json_data['barber_id']);

        $strSQL ="SELECT * FROM tbl_barber ";
        if($barber_id  != ""){
            $strSQL = $strSQL." WHERE barber_id  = '".$barber_id ."' ";
        }
        $strSQL = $strSQL." ORDER BY barber_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="POST"){

        @$names = trim($json_data['names']);
        @$age   = trim($json_data['age']);
        @$phone = trim($json_data['phone']);
        @$email = trim($json_data['email']);
        @$images= trim($json_data['images']);

        if($names=="" || $age=="" || $phone=="" || $email=="" || $images==""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $imageSplit = explode(',',trim($json_data['images']));
        $file_type = explode("image/", $imageSplit[0]);
        $image_base64 = base64_decode($imageSplit[1]);
        $fileName = 'bbimage-'.rand().'.png';
        $file = UPLOAD_DIR . $fileName;
        @file_put_contents($file, $image_base64);

        
        $strSQL = "INSERT INTO tbl_barber (names,age,phone,email,images) VALUES( '".$names."','".$age."','".$phone."','".$email."','".$fileName."' )";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
        
    }

    if($mode=="PUT"){
        
        @$barber_id = trim($json_data['barber_id']);
        @$names = trim($json_data['names']);
        @$age   = trim($json_data['age']);
        @$phone = trim($json_data['phone']);
        @$email = trim($json_data['email']);
        @$images= trim($json_data['images']);
        @$flag= trim($json_data['flag']);


        if($names=="" || $age=="" || $phone=="" || $email=="" || $barber_id=="" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        if($flag==TRUE){
            if($images == ""){
                echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
                return 0;
            }
        }

        $strSQL = "UPDATE tbl_barber SET names='".$names."',age='".$age."',phone='".$phone."',email='".$email."' ";

        if($images != ""){
            $imageSplit = explode(',',trim($json_data['images']));
            $file_type = explode("image/", $imageSplit[0]);
            $image_base64 = base64_decode($imageSplit[1]);
            $fileName = 'bbimage-'.rand().'.png';
            $file = UPLOAD_DIR . $fileName;
            @file_put_contents($file, $image_base64);
            $strSQL = $strSQL.",images='".$fileName."' ";
        }

        $strSQL = $strSQL."WHERE barber_id = '".$barber_id."' ";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }
    }

    if($mode=="DELETE"){
        @$barber_id = trim($json_data['barber_id']);
      
        if($barber_id == "" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>NULL));
            return 0;
        }else{

            $strcheck ="SELECT * FROM tbl_time WHERE barber_id = '".$barber_id."' ";
            if ($result=mysqli_query($conn,$strcheck)) {
                if(mysqli_num_rows($result)>0){
                    echo json_encode(array("status"=>200,"message"=>"USED","datas"=>NULL));
                    return 0;
                }
            }
          
            $sql = "DELETE FROM tbl_barber WHERE barber_id = '".$barber_id."' ";
            if ($conn->query($sql)) {
                echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
                return 0;
            }else{
                echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
                return 0;
            }

        }
        
    }

}
?>