<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="TODAY"){
       
        $date = date("Y-m-d");

        $strSQL = "SELECT *,tb3.names AS services_name,tb5.names AS barber_name,tb1.book_id AS book_ids,tb1.create_time AS booking_times FROM tbl_book tb1
        INNER JOIN user tb2 ON tb1.users_id = tb2.id
        INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
        INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id 
        INNER JOIN tbl_barber tb5 ON tb4.barber_id  = tb5.barber_id 
        LEFT JOIN tbl_approve_work tb6 ON tb1.book_id  = tb6.book_id 
        WHERE tb4.date_time LIKE '".$date."' AND tb1.book_status = 'APPROVE' AND approve_id  IS NULL
        ";

        $strSQL = $strSQL." ORDER BY book_time ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="APPROVE"){
       
        $date = date("Y-m-d");
        
        $strSQL = "SELECT *,tb3.names AS services_name,tb5.names AS barber_name,tb1.book_id AS book_ids,tb1.create_time AS booking_times  FROM tbl_approve_work  tb_main
        INNER JOIN tbl_book tb1 ON tb_main.book_id = tb1.book_id
        INNER JOIN user tb2 ON tb1.users_id = tb2.id
        INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
        INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id 
        INNER JOIN tbl_barber tb5 ON tb4.barber_id  = tb5.barber_id 
        WHERE tb1.book_status = 'APPROVE'
        ";

        $strSQL = $strSQL." ORDER BY tb4.date_time ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;

    }

    if($mode=="EXPIRE"){
       
        $date = date("Y-m-d");
        
        $strSQL = "SELECT *,tb3.names AS services_name,tb5.names AS barber_name,tb1.book_id AS book_ids,tb1.create_time AS booking_times FROM tbl_book tb1
        INNER JOIN user tb2 ON tb1.users_id = tb2.id
        INNER JOIN tbl_services tb3 ON tb1.services_id  = tb3.services_id 
        INNER JOIN tbl_time tb4 ON tb1.time_id  = tb4.time_id 
        INNER JOIN tbl_barber tb5 ON tb4.barber_id  = tb5.barber_id 
        LEFT JOIN tbl_approve_work tb6 ON tb1.book_id  = tb6.book_id 
        WHERE tb4.date_time < '".$date."' AND tb1.book_status = 'APPROVE' AND approve_id  IS NULL
        ";

        $strSQL = $strSQL." ORDER BY tb4.date_time ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
        
    }

    if($mode=="APPROVE_BOOK"){
       
        @$book_id = trim($json_data['book_id']);
        
        $strSQL = "INSERT INTO tbl_approve_work (book_id) VALUES( '".$book_id."' )";
        if ($conn->query($strSQL) === TRUE) {
            echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
            return 0;
        }else{
            echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
            return 0;
        }

        
    }

}
?>