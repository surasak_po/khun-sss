<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GET"){
        @$id   = trim($json_data['id']);

        $strSQL ="SELECT * FROM user ";
        if($id  != ""){
            $strSQL = $strSQL." WHERE id  = '".$id ."'  ";
        }else{
            $strSQL = $strSQL." WHERE  userlevel = 'member' ";
        }
        $strSQL = $strSQL." ORDER BY id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="DELETE"){
        @$id   = trim($json_data['id']);
        if($id == "" ){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>NULL));
            return 0;
        }else{
            $sql = "DELETE FROM user WHERE id = '".$id."' ";
            if ($conn->query($sql)) {
                echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
                return 0;
            }else{
                echo json_encode(array("status"=>400,"message"=>"ERROR","datas"=>NULL));
                return 0;
            }
        }
    }
}
?>