<?php 
require_once("./connection.php");
define('UPLOAD_DIR', '../upload/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
$content = @file_get_contents('php://input'); 
$json_data = @json_decode($content, true);
@$mode  = trim($json_data['mode']);  

if($_SERVER["REQUEST_METHOD"]=="POST"){ 

    if($mode=="GET"){
        @$time_id  = trim($json_data['time_id']);

        $strSQL ="SELECT * FROM tbl_time ";
        if($time_id  != ""){
            $strSQL = $strSQL." WHERE time_id  = '".$time_id ."' ";
        }
        $strSQL = $strSQL." ORDER BY time_id ASC";

        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="GET_BY_BARBER"){
        @$barber_id  = trim($json_data['barber_id']);
        @$date  = trim($json_data['date']);


        if($barber_id == "" || $date == ""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }

        $strSQL ="SELECT tb1.time_id,tb1.date_time ,tb1.time_str,tb1.barber_id,tb2.book_status  FROM tbl_time tb1 
        LEFT JOIN tbl_book tb2 ON tb1.time_id = tb2.time_id
        WHERE tb1.date_time LIKE '%".$date."%' AND tb1.barber_id = '".$barber_id."'   ";

        $strSQL = $strSQL." ORDER BY tb1.time_id ASC";
        $sth = mysqli_query($conn,$strSQL);
        $rows = array();
        while($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>$rows));
        return 0;
    }

    if($mode=="SET_TIME_BARBER"){

        @$barber_id  = trim($json_data['barber_id']);
        @$date  = trim($json_data['date']);
        @$times  = $json_data['times'];

        if($barber_id == "" || $date == ""){
            echo json_encode(array("status"=>400,"message"=>"REQUIRE","datas"=>[]));
            return 0;
        }
        
        //CLEAR TIME 
        $strSQLDelete2 ="DELETE tbl_time FROM tbl_time LEFT JOIN tbl_book ON tbl_time.time_id = tbl_book.time_id 
        WHERE tbl_time.date_time LIKE '%".$date."%' AND tbl_time.barber_id = '".$barber_id."' 
        AND tbl_book.book_status IS NULL";
        $sthDelete2 = mysqli_query($conn,$strSQLDelete2);
        if($sthDelete2){
            for ($x = 0; $x < count($times); $x++) {
                $strSQLGetData = "SELECT * FROM tbl_time 
                INNER JOIN tbl_book ON tbl_book.time_id = tbl_time.time_id
                WHERE time_str='".$times[$x]."' AND date_time LIKE '%".$date."%' ";
                $sthQuery = mysqli_query($conn,$strSQLGetData);
                $rowsData = array();
                while($r = mysqli_fetch_assoc($sthQuery)) {
                    $rowsData[] = $r;
                }
                if (count($rowsData) <= 0) {
                    $strSQLInsert ="INSERT INTO tbl_time(date_time,time_str,barber_id) 
                    VALUES('".$date."','".$times[$x]."','".$barber_id."')";
                    $result = mysqli_query($conn,$strSQLInsert);
                }
            }
        }
        echo json_encode(array("status"=>200,"message"=>"SUCCESS","datas"=>NULL));
        return 0;
    }
}
?>