<?php 
session_start();
$_SESSION['page'] = '8';

if (!$_SESSION['userid']) {
    header("Location: login.php");
} 
?>

<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- IMPORTSCRIPT -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <!-- END -->
        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
        
    </head>

    <body class="custom-body-color">
        <!-- Menu -->
        <?php include 'navbarV2.php'; ?>
        <!-- Menu -->
       

        <!-- -ข้อมูลทรงผม -->
        <div class="section primary-section custom-nav-margin" id="hair">
            <div class="container">
                <div class="text-center mb-4">
                    <h2 style="color:  #9ea86b;"><img src="images/pole.png"> ข้อมูลทรงผม <img src="images/pole.png"></h2>
                    <img src="images/line.png">
                </div>
                <div class="row">
                    <div class="col-12 text-end text-main">
                        <span class="cursor-pointer fs-20" id="btn_addBarber">เพิ่ม</span>
                    </div>
                </div>
                    <table class="w3-table table text-main">
                        <thead>
                            <tr class="w3-border">
                                <td><center>ชื่อทรง</center></td>
                                <td ><center>รูปภาพ</center></td>
                                <td><center>แก้ไขข้อมูล</center></td>
                                <td><center>ลบข้อมูล</center></td>
                            </tr>
                        </thead>
                        <tbody id="table_content">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
         <!-- footer -->
         <div style="padding-top: 220px;background-color: #181A1C;"></div>
            <div style="padding-top: 30px;background-color: #181A1C;">
                    <center>
                            <img src="images/slider01.png" width="120">
                            <h4 style="color:  #9ea86b;"><img src="images/pole.png"> Khuntong Barber <img src="images/pole.png"></h4>
                            <img src="images/line.png">
                            <a href="" style="color:  #9ea86b;"><h5 >ระบบบริหารจัดการธุรกิจร้านตัดผมออนไลน์ || © 2021 Khuntong Barber Management System</h5></a>
                            <br>
                    </center>
                </div>

        <div class="modal fade" id="addModal" tabindex="-1"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">เพิ่มข้อมูล</h5>
                </div>
                <div class="modal-body">
                    <div id="uploadAdd" class="">
                        <div class="text-center d-flex justify-content-center">
                            <div class="text-center">
                                <input type="file"
                                id="avatarAdd" name="avatarAdd"
                                onchange="UploadFile(this)"
                                accept="image/png, image/jpeg"
                                hidden>
                                <label  class="upload-content cursor-pointer" for="avatarAdd">
                                    +
                                </label >
                            </div>
                        </div>
                    </div>
                    <div id="showImageAdd" class="d-none">
                        <div class="d-flex  justify-content-center">
                            <div class="image-content text-center" >
                                <img id="imageAdd"
                                    src=""
                                >
                                <div class="fw-bolder text-danger cursor-pointer text-delete-image" >
                                    <span onclick="deleteAddImage()">ลบ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="row">
                        <div class="col-12">
                            <label class="text-label">ชื่อ</label>
                            <input type="text" class="form-control" id="txt_name" placeholder="ชื่อ" maxlength="50">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="closeAddModal()">ปิด</button>
                    <button type="button" class="btn btn-primary" onclick="addData()">เพิ่ม</button>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editModal" tabindex="-1"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">รายละเอียดข้อมูล</h5>
                </div>
                <div class="modal-body">
                    <div id="uploadEdit" class=" d-none">
                        <div class="text-center d-flex justify-content-center">
                            <div class="text-center">
                                <input type="file"
                                id="avatarEdit" name="avatarEdit"
                                onchange="UploadFileEdit(this)"
                                accept="image/png, image/jpeg"
                                hidden>
                                <label  class="upload-content cursor-pointer" for="avatarEdit">
                                    +
                                </label >
                            </div>
                        </div>
                    </div>
                    <div id="showImageEdit" class="">
                        <div class="d-flex justify-content-center ">
                            <div class="image-content text-center" >
                                <img id="imageEdit"
                                    src=""
                                >
                                <div class="fw-bolder text-danger cursor-pointer text-delete-image" >
                                    <span onclick="deleteEditImage()">ลบ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="row">
                        <div class="col-12">
                            <label class="text-label">ชื่อ</label>
                            <input type="text" class="form-control" id="txt_name_edit" placeholder="ชื่อ" maxlength="50">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="closeeditModal()">ปิด</button>
                    <button type="button" class="btn btn-primary" onclick="updateData()">บันทึก</button>
                </div>
                </div>
            </div>
        </div>

    <!-- CUSTOMJS -->
    <script src="./vendor2/jquery/jquery.min.js"></script>
    <script src="./vendor2/jquery-easing/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./JsScript/adminhair.js"></script>
    <link rel="stylesheet" type="text/css" href="css/upload.css" />
    <link href="./customCss/services.css" rel="stylesheet">
    <!-- END -->
    </body>
</html>