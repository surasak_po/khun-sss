<footer class="bg-dark-custom text-center text-white custom-footer-margin">
  <!-- Grid container -->
  <div class="container p-4">
    <div class="text-center">
      <img src="./images/slider01.png" width="120">
    </div>
    <div class="text-center text-main">
      <img src="./images/pole.png">
      Khuntong Barber 
      <img src="./images/pole.png">
    </div>
    <div class="text-center">
      <img src="./images/line.png" >
    </div>
    <div class="text-center text-main">
     <h5>ระบบบริหารจัดการธุรกิจร้านตัดผมออนไลน์ || © 2021 Khuntong Barber Management System</h5>
    </div>
  </div>
</footer>