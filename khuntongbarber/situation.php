<?php 

    session_start();

    if (!$_SESSION['userid']) {
        header("Location: login.php");
    } else {

    require_once('connection.php');

    if (isset($_REQUEST['update_id'])) {
        try {
            $id = $_REQUEST['update_id'];
            $select_stmt = $db->prepare('SELECT * FROM user WHERE id = :id');
            $select_stmt->bindParam(":id", $id);
            $select_stmt->execute();
            $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);
        } catch(PDOException $e) {
            $e->getMessage();
        }
    }   

    if (isset($_REQUEST['btn_update'])) {
        try {

            $username = $_REQUEST['username'];
            $password = $_REQUEST['password'];
            $email = $_REQUEST['email'];
            $phone = $_REQUEST['phone'];

            if (!isset($errorMsg)) {
                $update_stmt = $db->prepare("UPDATE user SET username = :username_up, password = :password_up, email = :email_up, phone = :phone_up, WHERE id = :id");
                $update_stmt->bindParam(':username_up', $username);
                $update_stmt->bindParam(':password_up', $password);
                $update_stmt->bindParam(':email_up', $email);
                $update_stmt->bindParam(':phone_up', $phone);
                $update_stmt->bindParam(':id', $id);

            if ($update_stmt->execute()) {
                header("Location: admin.php");
            }
            }
            
        } catch(PDOException $e) {
            $e->getMessage();
        }
    }


?>

<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
        
    </head>
    <style>
        .buttonsubmit, .buttonsubmit:visited, .buttonsubmit:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #9ea86b;
                padding: 15px 30px;
                font-size: 17px;
                width: 105px;
                height: 52px;
                border-radius: 12px;
                line-height: auto;              
            }
                .buttonsubmit:hover, .buttonsubmit:active {
                    background-color: #181A1C;
                    color: #efeae1;
                                    }

            .buttoncancle, .buttoncancle:visited, .buttoncancle:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #181A1C;
                padding: 15px 30px;
                font-size: 17px;
                width: 105px;
                height: 52px;
                border-radius: 12px;
                line-height: auto;              
            }

            .buttoncancle:hover, .buttoncancle:active {
                    background-color: #9ea86b;
                    color: #9ea86b;
                                    }
               

            .buttonmodal, .buttonmodal:visited, .buttonmodal:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #9ea86b;
                padding: 15px 30px;
                font-size: 17px;
                width: 92px;
                height: 42px;
                line-height: auto;   
                border-radius: 12px;
                padding-left: 22px; 
                padding-top: 10px;          
            }
                .buttonmodal:hover, .buttonmodal:active {
                    background-color: #181A1C;
                    color: #181A1C;
                                    }

                .buttonmodalcancle, .buttonmodalcancle:visited, .buttonmodalcancle:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #181A1C;
                padding: 15px 30px;
                font-size: 17px;
                width: 92px;
                height: 42px;
                line-height: auto;   
                border-radius: 12px;
                padding-left: 22px; 
                padding-top: 10px;          
            }
                .buttonmodalcancle:hover, .buttonmodalcancle:active {
                    background-color: #9ea86b;
                    color: #9ea86b;
                                    }


                select {
                    height: 40px;
                }
                .dss {
                            display: none;
                        }
                                    
                

        
    </style>
    <body>
    
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

                        <img src="images/logo.png" width="180">
                        <!-- Logo -->

                    <!-- Button -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Menu -->
                    <div class="nav-collapse collapse pull-right">
                    <ul class="nav" id="top-navigation">
                            <li><a href="user.php">หน้าแรก</a></li>
                            <li><a href="user.php#service">บริการ</a></li>
                            <li><a href="user.php#barber">ช่างตัดผม</a></li>
                            <li><a href="user.php#hair">ทรงผม</a></li>
                            <li><a href="bookbarber.php">จองคิว</a></li>
                            <li  class="active"><a href="situation.php"><img src="images/bell.png"width="25"></a></li>
                            <li><a href=""><img src="images/user.png"width="25"> <?php echo $_SESSION['user']; ?></a></li>
                            <li><a href="logout.php"><img src="images/logout.png"width="25"> ออกจากระบบ</a></li>
                        </ul>
                    </div>
                    <!-- Menu -->
                </div>
            </div>
        </div>

        <!-- Start home section -->
        <div id="home" style="background-color: #efeae1;">
                <div class="triangle"></div>
                    <div class="mask"></div>
                <div class="container">
                                <center>
                                 <div class="title">
                                    <h1><img src="images/book.png"></h1>
                                    <h3 style="color: black;"><img src="images/pole.png"> รายการจองคิวของลูกค้า <img src="images/pole.png"></h3>
                                </div>
                                </center><br>

                                <form method="post">
                    <table class="w3-table">
                        <thead>
                            <tr class="w3-border" style="font-size: 16px;font-weight: bold;color:#efeae1;background-color: #181A1C;">
                                <td><center>ชื่อผู้ใช้</center></td>
                                <td ><center>วัน</center></td>
                                <td><center>เวลา</center></td>
                                <td><center>รายการให้บริการ</center></td>
                                <td><center>ช่างให้บริการ</center></td>
                                <td><center>ทรงผม</center></td>
                                <td><center>ค่ามัดจำ</center></td>
                                <td><center>สลิปการโอน</center></td>
                                <td><center>สถานะ</center></td>
                                <td><center>แก้ไข / ยกเลิก</center></td>
                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                            
                                         $user =  $_SESSION['user'];

                                $select_stmt = $db->prepare("SELECT * FROM book WHERE book_by = '$user' "); 
                                $select_stmt->execute();

                                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                                <tr class="w3-border">
                                    <td><center><?php echo $row['book_user']; ?></center></td>
                                    <td><center><?php echo $row['book_date']; ?></center></td>     
                                    <td><center><?php echo $row['book_time']; ?></center></td>    
                                    <td><center><?php echo $row['book_service']; ?></center></td>
                                    <td><center><?php echo $row['book_barber']; ?></center></td>
                                    <td><center><?php echo $row['book_hair']; ?></center></td>
                                    <td><center><?php echo $row['book_pay']; ?></center></td>
                                    <td><center><img src="upload/<?php echo $row['book_image']; ?>" width="200px" height="200px" alt=""></center></td>
                                    <td><center><?php echo $row['book_situation']; ?></center></td>
                                    <td><center><a href="editsituation.php?update_id=<?php echo $row['book_id']; ?>" class="btn btn-warning" >แก้ไข</a>
                                    <a href="" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter" >ยกเลิก</a></center></td>
                                </tr>


                                 <!-- modal -->
                                    <div class="modal fade dss" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header  justify-content-center">
                                                                <img src="images/delete.gif" width="360px" style="padding-left: 170px;">
                                                                <h3 class="text-center"style="color:black;">warning!</h3>
                                                                <h5 class="text-center"style="color:black;">ต้องการลบข้อมูลการจองคิวหรือไม่</h5>
                                                            </div>
                                                        
                                                            
                                                            <div class="modal-footer">

                                                            <button onclick="location.href='delete_book_order.php?book_id=<?php echo $row['book_id']; ?>'" type="button" class="buttonmodal"style="color:white;">ลบ</button>
                                                                <button type="button" class="buttonmodalcancle" data-dismiss="modal"style="color:white;">ปิด</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                        </div>
                            <?php } ?>
                        </tbody>
                    </table>
                    </form>


                </div>
          
        </div>
 <!-- footer -->
 <div style="padding-top: 420px;background-color: #efeae1;"></div>
 <div style="padding-top: 30px;background-color: #181A1C;">
                    <center>
                            <img src="images/slider01.png" width="120">
                            <h4 style="color:  #9ea86b;"><img src="images/pole.png"> Khuntong Barber <img src="images/pole.png"></h4>
                            <img src="images/line.png">
                            <a href="" style="color:  #9ea86b;"><h5 >ระบบบริหารจัดการธุรกิจร้านตัดผมออนไลน์ || © 2021 Khuntong Barber Management System</h5></a>
                            <br>
                    </center>
                </div>

        
        <!-- javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>

<?php } ?>