<?php 

    session_start();

    require_once "connection.php";

    if (isset($_POST['submit'])) {

        $service_type = $_POST['service_type'];
        $service_price = $_POST['service_price'];
		$service_time = $_POST['service_time'];

        $query = "INSERT INTO service (service_type, service_price, service_time)
                	VALUE ('$service_type', '$service_price', '$service_time')";
		mysqli_query($conn,$query) or die(mysqli_error($conn));
			
        header("Location: adminservice.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Khuntong Barbershop</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/icon.png">
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-service.jpg');">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
			<form class="login100-form validate-form flex-sb flex-w" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
				<a class="nav-brand"><img src="images/logo-login.png">
					<div class="p-t-31 p-b-9">
						<div class="row">
						<label for="name" class="col-sm-3 control-label">รายการ</label>
						<div class="col-sm-9">
							<input type="text" name="service_type" class="form-control">
						</div>
						</div>
					</div>

					<div class="p-t-31 p-b-9">
						<div class="row">
						<label for="name" class="col-sm-3 control-label">ราคา</label>
						<div class="col-sm-9">
							<input type="text" name="service_price" class="form-control">
						</div>
						</div>
					</div>

					<div class="p-t-31 p-b-9">
						<div class="row">
						<label for="name" class="col-sm-3 control-label">เวลา</label>
						<div class="col-sm-9">
							<input type="text" name="service_time" class="form-control">
						</div>
						</div>
					</div>

					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn" type = "submit" name = "submit" value="Submit" onclick="alert('เพิ่มข้อมูล')">
							เพิ่มข้อมูล
						</button>
					</div>
					<div class="w-full text-center p-t-55">
						<a href="adminservice.php"><img src="images/back1.png" width="40"></a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>

</body>
</html>