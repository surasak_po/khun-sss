<nav class="navbar navbar-expand-sm  fixed-top nav-cus-background ">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="./images/logo.png" alt=""width="150" class="d-inline-block align-text-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav d-flex flex-grow-1">
        <li class="nav-item">
          <a class="nav-link" href="user.php">หน้าแรก</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="user.php#service">บริการ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="user.php#barber">ช่างตัดผม</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="user.php#hair">ทรงผม</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="bookbarber.php">จองคิว</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="bookdetail.php">การจองคิวของฉัน</a>
        </li>
      </ul>
      <form class="d-flex ">
        <ul class="navbar-nav d-flex flex-grow-0">
          <li class="nav-item">
            <a class="nav-link" href="admin_mainV2.php">
              <img src="./images/user.png" width="25">
              <?php echo $_SESSION['user']; ?>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">
              <img src="./images/logout.png" width="25">
              ออกจากระบบ
            </a>
          </li>
        </ul>
      </form>
    </div>
  </div>
</nav>