<?php
    session_start();

    require_once('connection.php');

    if (!$_SESSION['userid'] || !$_REQUEST['id']) {
        header("Location: situation.php");
    } else{

   

?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
        
    </head>
    <style>
        .buttonsubmit, .buttonsubmit:visited, .buttonsubmit:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #9ea86b;
                padding: 15px 30px;
                font-size: 17px;
                width: 105px;
                height: 52px;
                border-radius: 12px;
                line-height: auto;              
            }
                .buttonsubmit:hover, .buttonsubmit:active {
                    background-color: #181A1C;
                    color: #efeae1;
                                    }

            .buttoncancle, .buttoncancle:visited, .buttoncancle:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #181A1C;
                padding: 15px 30px;
                font-size: 17px;
                width: 105px;
                height: 52px;
                border-radius: 12px;
                line-height: auto;              
            }

            .buttoncancle:hover, .buttoncancle:active {
                    background-color: #9ea86b;
                    color: #9ea86b;
                                    }
               

            .buttonmodal, .buttonmodal:visited, .buttonmodal:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #9ea86b;
                padding: 15px 30px;
                font-size: 17px;
                width: 92px;
                height: 42px;
                line-height: auto;   
                border-radius: 12px;
                padding-left: 22px; 
                padding-top: 10px;          
            }
                .buttonmodal:hover, .buttonmodal:active {
                    background-color: #181A1C;
                    color: #181A1C;
                                    }

                .buttonmodalcancle, .buttonmodalcancle:visited, .buttonmodalcancle:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #181A1C;
                padding: 15px 30px;
                font-size: 17px;
                width: 92px;
                height: 42px;
                line-height: auto;   
                border-radius: 12px;
                padding-left: 22px; 
                padding-top: 10px;          
            }
                .buttonmodalcancle:hover, .buttonmodalcancle:active {
                    background-color: #9ea86b;
                    color: #9ea86b;
                                    }


                select {
                    height: 40px;
                }
                .dss {
                            display: none;
                        }
                        input{
                            height: 30px;
                        }

                                    
                

        
    </style>
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

                        <img src="images/logo.png" width="180">
                        <!-- Logo -->

                    <!-- Button -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Menu -->
                    <div class="nav-collapse collapse pull-right">
                    <ul class="nav" id="top-navigation">
                        <li><a href="admin.php">รายการจองคิว</a></li>
                            <li><a href="adminuser.php">ข้อมูลลูกค้า</a></li>
                            <li><a href="adminservice.php">ข้อมูลบริการ</a></li>
                            <li><a href="adminbarber.php">ข้อมูลช่าง</a></li>
                            <li><a href="adminhair.php">ข้อมูลทรงผม</a></li>
                            <li><a href="adminaccount.php">บัญชีร้าน</a></li>
                            <li><a href="admin.php"><img src="images/user.png"width="25"> <?php echo $_SESSION['user']; ?></a></li>
                            <li><a href="logout.php"><img src="images/logout.png"width="25"> ออกจากระบบ</a></li>
                        </ul>
                    </div>
                    <!-- Menu -->
                </div>
            </div>
        </div>
        

		<!-- ชำระเงิน -->
        <div style="background-color: #efeae1;">
                <div class="triangle"></div>
                <div class="mask"></div>
            <div class="container">
                <div class="title">
                <center>
                    <h1><img src="images/book.png"></h1>
                   
                </center>
                </div><br>
                    <div class="row-fluid">
                        
                    <?php 
                   
                                 $id=$_REQUEST['id'];
                                $select_stmt = $db->prepare("SELECT * FROM schedule WHERE id='$id'"); 
                                $select_stmt->execute();

                                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {?>
                                <?php
                                                        
                                                        if($row['barber'] == "1"){ 
                                                                        $barber = ' ช่างฟรองซ์ ';
                                                           
                                                            } 
                                                            else if($row['barber'] == "2" ){
                                                                $barber = ' ช่างปาร์ค ';
                                                            }

                                                            if($row['status'] == "1"){ 
                                                                $status = ' เปิดให้บริการ ';
                                                   
                                                            } 
                                                            else if($row['status'] == "2" ){
                                                                $status = ' ปิดให้บริการ ';
                                                            }

                                                ?>
                                <div class="span3 price-column" style="background-color: #efeae1;"></div>
                                        <div class="span6 price-column">
                                        <h3> แก้ไขข้อมูลตารางช่าง </h3>
                                            <form method="post" action="update_admintable_byid.php" class="form-horizontal" enctype="multipart/form-data"><br>
                                            <br>
                                                <h4 style="margin-right: 10px;"> รหัสช่าง : <input class="text-center" name="id" value="<?php echo $row['id']; ?>" readonly ></h4>
                                                <h4 style="margin-right: 20px;"> ช่างตัดผม : <input class="text-center" name="barber" value="<?php echo $barber ?>" readonly ></h4>
                                                <h4 style="margin-right: 45px;">วันที่และเวลา : <input class="text-center" name="dayandtime" value="<?php echo $row['dayandtime']; ?>" readonly ></h4>
                                                
                                               

                                                <h4>สถานะ :
                                                <select name="status">
                                                   
                                                   <option value="<?php echo $row['status']; ?>">เลือก : <?php echo  $status ?> </option>
                                                   <option value="1">เปิดให้บริการ</option>
                                                   <option value="2">ปิดให้บริการ</option>
                                          
                                            </select>
                                                    
                                                </h4>
                                                <br><br><br>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="centered service ">
                                                            <div class=" zoom-in">
                                                            <button type="submit"  class="buttonsubmit " style="color:white;">ยืนยัน</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" data-toggle="modal" data-target="#exampleModalCenter" class="buttoncancle" style="color:white;">ยกเลิก</button>
                                                            
                                                            
                                                        
                                                            </div>
                                                        
                                                        </div> 
                                                        </div>       
                                                    
                                                </div>
                                                
                                                <img src="images/line.png"><br><br>
                                            
                                        </form>
                                    <div style="margin-bottom: 95px;"></div>
                                    </div>

                                 <div class="span3 price-column" style="background-color: #efeae1;"></div>
                               
                            <?php } ?>



						
                    </div>
            </div>
     </div>
  <!-- footer -->
  <!-- footer -->
  <div style="padding-top: 220px;background-color: #efeae1;"></div>
  <div style="padding-top: 30px;background-color: #181A1C;">
                    <center>
                            <img src="images/slider01.png" width="120">
                            <h4 style="color:  #9ea86b;"><img src="images/pole.png"> Khuntong Barber <img src="images/pole.png"></h4>
                            <img src="images/line.png">
                            <a href="" style="color:  #9ea86b;"><h5 >ระบบบริหารจัดการธุรกิจร้านตัดผมออนไลน์ || © 2021 Khuntong Barber Management System</h5></a>
                            <br>
                    </center>
                </div>
       <!-- modal -->
       <div class="modal fade dss" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                            <div class="modal-header  justify-content-center">
                                <img src="images/animation.gif" width="360px" style="padding-left: 80px;">
                                <h3 class="text-center"style="color:black;">warning!</h3>
                                <h5 class="text-center"style="color:black;">ข้อมูลของลูกค้าจะไม่ถูกบันทึกต้องการยกเลิกการแก้ไขหรือไม่</h5>
                            </div>
                        
                            
                            <div class="modal-footer">

                            <button onclick="location.href='admin_main.php'" type="button" class="buttonmodal"style="color:white;">ยกเลิก</button>
                                <button type="button" class="buttonmodalcancle" data-dismiss="modal"style="color:white;">ปิด</button>
                            </div>
                            </div>
                        </div>
        </div>
        <!-- javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
<?php } ?>