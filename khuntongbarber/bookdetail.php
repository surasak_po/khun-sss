<?php 

    session_start();

    require_once('connection.php');

    if (!$_SESSION['userid']) {
        header("Location: login.php");
    } else {
?>

<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        
        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
        
            <style>
                     
            </style>

    </head>
<script>
    window.userId = <?php echo $_SESSION['userid']; ?>;
</script>

    <body class="custom-body-color">
        <?php include 'navbar_user.php'; ?>
       
        <!-- -ข้อมูลบริการ -->
        <div class="section primary-section custom-nav-margin" id="hair">
            <div class="container">
                <div class="text-center mb-4">
                    <h2 style="color:  #9ea86b;"><img src="images/pole.png"> ข้อมูลการจอง <img src="images/pole.png"></h2>
                    <img src="images/line.png">
                </div>
                <div class="row text-main">
                </div>
                    <table class="w3-table table text-main">
                        <thead>
                            <tr class="w3-border">
                                <td><center>ชื่อบริการ</center></td>
                                <td ><center>ค่าบริการ</center></td>
                                <td><center>ช่าง</center></td>
                                <td><center>วันที่ทำรายการ</center></td>
                                <td><center>วันที่จอง</center></td>
                                <td><center>เวลาจอง</center></td>
                                <td><center>สถานะ</center></td>
                            </tr>
                        </thead>
                        <tbody id="table_content">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    <!-- footer -->
    <?php include 'footer.php'; ?>
    <!-- END -->

<!-- MODAL -->
<div class="modal fade" id="modal_mng_times" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">จองคิว</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-6">
                  <label class="fw-bolder">วันที่ : <span id="txt_mng_date"> </span></label>
              </div>
              <div class="col-6">
                  <label class="fw-bolder">บริการ :</label>
                  <select class="form-select" id="sel_service">
                  </select>
              </div>
          </div>
          <div class="row">
            <div class="col-6"></div>
            <div class="col-6">
                  <label class="fw-bolder">ช่าง :</label>
                  <select class="form-select" id="sel_barber">
                  </select>
              </div>
          </div>
          <div class="row">
              <div class="col-6">
                  <label class="fw-bolder">รออนุมัติ : <span class="badge bg-warning"> </span></label>
                  <label class="fw-bolder">อนุมัติ : <span class="badge bg-success"> </span></label>
              </div>
          </div>
          <div class="row mt-2">
              <div class="col-12">
                 <table class="table table-bordered table-responsive-sm">
                     <tr>
                        <td class="" id="check_11_block">
                         <div class="form-check">
                           <label class="form-check-label" for="check_11"><input class="form-check-input" type="radio" name="rad_times" value="11.00" id="check_11">11.00 น</label>
                         </div>
                        </td>
                        <td class="" id="check_13_block">
                         <div class="form-check">
                           <label class="form-check-label" for="check_13"><input class="form-check-input" type="radio" name="rad_times" value="13.00" id="check_13">13.00 น</label>
                         </div>
                        </td>
                        <td class="" id="check_14_block">
                         <div class="form-check">
                           <label class="form-check-label" for="check_14"><input class="form-check-input" type="radio" name="rad_times" value="14.00" id="check_14">14.00 น</label>
                         </div>
                        </td>
                        <td class="" id="check_15_block">
                         <div class="form-check">
                           <label class="form-check-label" for="check_15"><input class="form-check-input" type="radio" name="rad_times" value="15.00" id="check_15">15.00 น</label>
                         </div>
                        </td>
                        <td class="" id="check_16_block">
                         <div class="form-check">
                           <label class="form-check-label" for="check_16"><input class="form-check-input" type="radio" name="rad_times" value="16.00" id="check_16">16.00 น</label>
                         </div>
                        </td>
                        <td class="" id="check_17_block">
                          <div class="form-check">
                            <label class="form-check-label" for="check_17">
                            <input class="form-check-input" type="radio" name="rad_times" value="17.00" id="check_17">
                              17.00 น
                            </label>
                          </div>
                        </td>
                        <td class="" id="check_18_block">
                          <div class="form-check">
                            <label class="form-check-label" for="check_18">
                                <input class="form-check-input" type="radio" name="rad_times" value="18.00" id="check_18">18.00 น
                            </label>
                          </div>
                        </td>
                        <td class="" id="check_19_block">
                          <div class="form-check">
                            <label class="form-check-label" for="check_19"><input class="form-check-input" type="radio" name="rad_times" value="19.00" id="check_19">19.00 น</label>
                          </div>
                        </td>
                     </tr>
                     <tr>
                      <td class="" id="check_20_block">
                        <div class="form-check">
                          <label class="form-check-label" for="check_20"><input class="form-check-input" type="radio" name="rad_times" value="20.00" id="check_20">20.00 น</label>
                        </div>
                      </td>
                      <td class="" id="check_21_block">
                        <div class="form-check">
                          <label class="form-check-label" for="check_21"><input class="form-check-input" type="radio" name="rad_times" value="21.00" id="check_21">21.00 น</label>
                        </div>
                      </td>
                  </tr>
                 </table>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="btn_save_time"  data-bs-dismiss="modal">ยืนยัน</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_book_image" tabindex="-1"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">หลักฐานการจอง</h5>
                </div>
                <div class="modal-body">
                  <div class="row">
                        <div class="col-12">
                            <span>
                                <span class="fw-bolder">บริการ : </span>
                                <span id="lbl_service"></span>
                            </span>
                        </div>
                        <div class="col-12">
                            <span>
                                <span class="fw-bolder">ช่าง : </span>
                                <span id="lbl_barber"> </span>
                            </span>
                        </div>
                        <div class="col-12">
                            <span>
                                <span class="fw-bolder">เวลาจอง : </span>
                                <span id="lbl_book"> </span>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-12">
                            <div class="text-start badge-pay-detail">
                                สแกน QR เพิ่อโอนเข้าบัญชี 
                                และตรวจสอบใบเสร็จของท่านพร้อมแนบสลิปการโอน จากนั้นกดที่ยืนยันการจอง
                            </div>
                            <div class="text-center">
                                <h4 style="margin-right: 35px;">ค่ามัดจำ <input  class="text-center form-control" name="book_pay" value="50 บาท"readonly><br>
                            </div>
                        </div>
                        <div class="col-12">
                            <img style="object-fit: contain;" src="images/qr.jpg" width="447px" height="447x">
                        </div>
                    </div>
                

                    <div id="uploadAdd" class="">
                    
                        <div class="d-flex justify-content-center">
                           

                            <div class="text-center">
                                <input type="file"
                                id="avatarAdd" name="avatarAdd"
                                onchange="UploadFile(this)"
                                accept="image/png, image/jpeg"
                                hidden>
                                <label  class="upload-content cursor-pointer" for="avatarAdd">
                                    +
                                </label >
                            </div>
                        </div>
                    </div>
                    <div id="showImageAdd" class="d-none">
                        <div class="d-flex  justify-content-center">
                            <div class="image-content text-center" >
                                <img id="imageAdd"
                                    src=""
                                >
                                <div class="fw-bolder text-danger cursor-pointer text-delete-image" >
                                    <span onclick="deleteAddImage()">ลบ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    <button type="button" class="btn btn-primary" onclick="booking()">ยืนยันจอง</button>
                </div>
                </div>
            </div>
        </div>
<!-- ENDMODAL -->
        <!-- javascript -->
        <link href="./customCss/user.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/upload_book.css" />
        <script src="js/jquery.js"></script>
        <script src="./JsScript/middleware.js"></script>
        <script type="text/javascript" src="./JsScript/userbookdetail.js"></script>


    </body>
</html>

<?php } ?>