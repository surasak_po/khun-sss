<?php 

    session_start();

    require_once('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
            <style>
                    .modal-body {
                    position: relative;
                    max-height: 500px;
                    padding: 5px;
                    overflow-y: auto;
                }
                .dss {
                    display: none;
                }

            </style>
    </head>

    <body>

        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

                        <img src="images/logo.png" width="180">
                        <!-- Logo -->

                    <!-- Button -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Menu -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="#home">หน้าแรก</a></li>
                            <li><a href="#service">บริการ</a></li>
                            <li><a href="#barber">ช่างตัดผม</a></li>
                            <li><a href="#hair">ทรงผม</a></li>
                            <li><a href="login.php">เข้าสู่ระบบ</a></li>
                        </ul>
                    </div>
                    <!-- Menu -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
            <!-- ขุนทอง บาร์เบอร์ -->
            <div id="da-slider" class="da-slider">
                <div class="triangle"></div>
                    <div class="mask"></div>
                <div class="container">
                    <div class="da-slide">
                        <h2 class="fittext2">Khuntong Barber <img src="images/pole.png"></h2>
                        <h4>ขุนทอง บาร์เบอร์</h4>
                        <p>ก่อตั้งเมื่อวันที่ 8 กุมภาพันธ์ 2014 โดยนายสิทธิกร แก่นอากาศ เป็นร้านตัดผมร่วมสมัย ณ จังหวัดขอนแก่น ให้บริการเสริมแต่ง ออกแบบทรงผมให้กับผู้มาใช้บริการ ครอบคลุมทั้งการ ตัดผม เซ็ทผม ย้อมผม โกนหนวด อีกทั้งมีการตกแต่งร้านเข้ากับสมัยนิยม</p>
                        <a href="login.php" class="da-link button">จองคิว</a>  
                        <div class="da-img">
                            <img src="images/slider01.png" width="420">
                        </div>
                    </div>
                    <!-- เวลาทำการ -->
                    <div class="da-slide">
                        <h2>Business Hours <img src="images/pole.png"></h2>
                        <h4>เวลาทำการ</h4>
                        <p>
                            วันอาทิตย์&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;13:00 - 21:00<br>
                            วันจันทร์ - วันศุกร์&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11:00 - 21:00<br>
                            วันเสาร์&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12:00 - 21:00<br>
                            (กรณีร้านหยุดทำการจะแจ้งทางเพจร้าน ตามลิงค์ด้านล่าง)
                        </p>
                        <a href="" class="da-link button" data-toggle="modal" data-target="#facebookindex">Facebook</a> 
                        <div class="da-img">
                            <img src="images/slider02.png">
                        </div>
                    </div>
                    <!-- ตำแหน่งที่ตั้ง -->
                    <div class="da-slide">
                        <h2>Location <img src="images/pole.png"></h2>
                        <h4>ตำแหน่งที่ตั้ง</h4>
                        <p>
                        ร้านขุนทอง บาร์เบอร์ 525 ถนน หน้าเมือง ตำบล ในเมือง อำเภอ เมือง <br>
                        จังหวัด ขอนแก่น รหัสไปรษณีย์ 40000<br>
                        </p>
                        <a href="" class="da-link button" data-toggle="modal" data-target="#map">ขอทราบเส้นทาง</a>
                        <div class="da-img">
                            <img src="images/slider03.png">
                        </div>
                    </div>
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                </div>
            </div>
        </div>
        <!--สมัคร-->
        <div class="section primary-section">
            <div class="container">
            <center><h1><a href="register.php"><img src="images/register.jpg"></a></h1></center>
            </div>
        </div>
        <!-- บริการ -->
        <div class="section secondary-section" id="service">
            <div class="container">
                <center><h1><img src="images/titleservice.jpg"></h1></center>
                <div class="price-table row-fluid">
                    <div class="span4 price-column">
                        <h3>รายการให้บริการ <img src="images/barbershop.png"></h3>
                            <?php 
                                $select_stmt = $db->prepare('SELECT * FROM tbl_services'); 
                                $select_stmt->execute();

                                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                                <ul class="list">
                                    <li><center><?php echo $row['names']; ?></center></li>                     
                                </ul>
                            <?php } ?>
                    </div>
                    <div class="span4 price-column">
                        <h3>ราคาการให้บริการ <img src="images/money.png"></h3>
                            <?php 
                                $select_stmt = $db->prepare('SELECT * FROM tbl_services'); 
                                $select_stmt->execute();

                                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                                <ul class="list">
                                    <li><center><?php echo $row['price']; ?></center></li>                     
                                </ul>
                            <?php } ?>
                    </div>
                    <div class="span4 price-column">
                        <h3>เวลาในการใช้บริการ <img src="images/clock.png"></h3>
                            <?php 
                                $select_stmt = $db->prepare('SELECT * FROM tbl_services'); 
                                $select_stmt->execute();

                                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                                <ul class="list">
                                    <li><center><?php echo $row['duration']; ?></center></li>                     
                                </ul>
                            <?php } ?>
                    </div>
                </div>
            </div>
        </div>
   
        <!--ช่างตัดผม-->
        <div class="section secondary-section " id="barber">
            <div class="triangle"></div>
                <div class="container">
                    <div class="price-table row-fluid">
                        <div class="span12 price-column">
                                <h3>ช่าง <img src="images/clipper.png"></h3>
                                    <?php 
                                        $select_stmt = $db->prepare('SELECT * FROM tbl_barber'); 
                                        $select_stmt->execute();
                                        while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <br><center><img src="upload/<?php echo $row['images']; ?>" width="165px" height="165x" alt=""><br><br>
                                    <span>ช่าง :</span><span><?php echo $row['names']; ?></span><br>
                                    <img src="images/line.png">
                                </center>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ทรงผม -->
        <div class="section secondary-section " id="hair">
            <div class="triangle"></div>
                <div class="container">
                    <div class="price-table row-fluid">
                        <div class="span6 price-column">
                                <h3>ชื่อทรงผม <img src="images/razor.png"></h3>
                                    <?php 
                                        $select_stmt = $db->prepare('SELECT * FROM tbl_hair'); 
                                        $select_stmt->execute();
                                        while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <br><br><br><br><center>ทรงผม : <?php echo $row['names']; ?><br><br><br><br><br><img src="images/line.png"></center>           
                                <?php } ?>
                            </div>
                        <div class="span6 price-column">
                                <h3>แบบทรงผม <img src="images/clipper.png"></h3>
                                    <?php 
                                        $select_stmt = $db->prepare('SELECT * FROM tbl_hair'); 
                                        $select_stmt->execute();
                                        while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <br><center><img src="upload/<?php echo $row['images']; ?>" width="165px" height="165x" alt=""><br><br><img src="images/line.png"></center>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- youtube -->
        <div class="section primary-section">
            <div class="triangle"></div>
                <div class="container">
                    <center>
                    <iframe width="1160" height="315" src="https://www.youtube.com/embed/yM4bYyC1HU0?autoplay=1"></iframe>
                    </center>
                </div>
            </div>
        </div>
 <!-- footer -->
 <div style="padding-top: 220px;background-color: #181A1C;"></div>
 <div style="padding-top: 30px;background-color: #181A1C;">
                    <center>
                            <img src="images/slider01.png" width="120">
                            <h4 style="color:  #9ea86b;"><img src="images/pole.png"> Khuntong Barber <img src="images/pole.png"></h4>
                            <img src="images/line.png">
                            <a href="" style="color:  #9ea86b;"><h5 >ระบบบริหารจัดการธุรกิจร้านตัดผมออนไลน์ || © 2021 Khuntong Barber Management System</h5></a>
                            <br>
                    </center>
                </div>



                 <!-- Modal Map-->
                 <div class="modal fade dss" id="map" data-backdrop="static"  data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="staticBackdropLabel">Map</h4>
                                
                            </div>
                            <div class="modal-body">
                            
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122468.16463028204!2d102.76249520246373!3d16.418215393275638!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x312289b75373bd33%3A0x6e463a67c44aa3b1!2z4LiC4Li44LiZ4LiX4Lit4LiH4Lia4Liy4Lij4LmM4LmA4Lia4Lit4Lij4LmM4LiK4Lit4Lib!5e0!3m2!1sen!2sth!4v1630351539225!5m2!1sen!2sth" width="540" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            
                            </div>
                            </div>
                        </div>
                 </div>

                <!-- Modal Facebook -->
                <div class="modal fade dss" id="facebookindex" data-backdrop="static"  data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="staticBackdropLabel">Facebook</h4>
                                
                            </div>
                            <div class="modal-body">
                            
                            <div class="fb-page" data-href="https://www.facebook.com/khuntongbarber/?ref=page_internal" data-tabs="timeline" data-width="500" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/khuntongbarber/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/khuntongbarber/?ref=page_internal">KhunTong Barber Shop</a></blockquote></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            
                            </div>
                            </div>
                        </div>
                    </div>

                    <!-- Div && javascript Modal facebook -->
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v12.0" nonce="C8OU8K7R"></script>
                <!-- javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>