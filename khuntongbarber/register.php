<!DOCTYPE html>
<html lang="en">
<head>
<title>Khuntong Barbershop</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/icon.png">
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

    <div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-register.jpg');">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
				<form class="login100-form validate-form flex-sb flex-w" action="register_checked.php" method="post">
					<a class="nav-brand"><img src="images/logo-login.png">
					<div class="p-t-31 p-b-9">
						<span class="txt1">
							ชื่อผู้ใช้
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "กรุณากรอกชื่อผู้ใช้">
						<input class="input100" type = "text" name = "username"  required>
						<span class="focus-input100"></span>
					</div>

                    <div class="p-t-13 p-b-9">
						<span class="txt1">
							อีเมล
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "กรุณากรอกอีเมล">
						<input class="input100" type = "email" name = "email" required>
						<span class="focus-input100"></span>
					</div>
					
					<div class="p-t-13 p-b-9">
						<span class="txt1">
							รหัสผ่าน 
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "กรุณากรอกรหัสผ่าน">
						<input class="input100" type = "password" name = "password" required>
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn" type = "submit" name = "submit" value="Submit">
							สมัครสมาชิก
						</button>
					</div>

					<div class="w-full text-center p-t-55">
						<span class="txt2">
                            เป็นสมาชิกแล้วใช่ไหม?
						</span>

						<a href="login.php" class="txt2 bo1">
							เข้าสู่ระบบ
						</a>

						<br><br><a href="index.php"><img src="images/home.png" width="40"></a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>

</body>
</html>