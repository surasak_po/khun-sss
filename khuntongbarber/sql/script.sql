--book จอง
--service บริการ services_id PK
CREATE TABLE tbl_barber (
    barber_id       INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    names           VARCHAR(128)  NOT NULL,
    age             VARCHAR(2)  NOT NULL,
    phone           VARCHAR(10)  NOT NULL,
    email           VARCHAR(256)  NOT NULL,
    images          TEXT        NOT NULL
)

CREATE TABLE tbl_hair (
    hair_id         INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    names           VARCHAR(128)  NOT NULL,
    images          TEXT  NOT NULL
)

CREATE TABLE tbl_services (
    services_id         INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    names               VARCHAR(128)  NOT NULL,
    price               INT(10)  NOT NULL,
    duration            INT(10)  NOT NULL
)

INSERT INTO tbl_services(names,price,duration) VALUES 
("เด็ก (ต่ำกว่า 18 ปี)",140,50),
("ผู้ใหญ่ (ต่ำกว่า 18 ปี)",200,50),
("โกนหนวด",60,30),
("เซ็ทผม",60,10),
("ย้อมผม",200,60)

CREATE TABLE tbl_time (
    time_id             INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    date_time           DATETIME  NOT NULL,
    time_str            VARCHAR(20)  NOT NULL, -- เวลา 11.00 
    barber_id           INT(6)  NOT NULL
)

CREATE TABLE tbl_book (
    book_id             INT(6)   UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    users_id            INT(20)       NOT NULL,
    services_id         INT(6)        NOT NULL,
    services_price      INT(10)       NOT NULL,
    services_duration   INT(10)       NOT NULL,
    time_id             INT(6)        NOT NULL,
    book_time           VARCHAR(20)   NOT NULL,
    images              TEXT          NOT NULL,
    create_time         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    book_status         VARCHAR(20)   NOT NULL DEFAULT 'WAIT_APPROVE', --APPROVE  --REJECT --ถ้า WAIT_APPROVE มาหลาย ROW จนกว่าจะ APPROVE ถึงจะไปกวาด เป็น REJECT ให้หมด
    deposit_price       INT(10)       NOT NULL DEFAULT 50
)


CREATE TABLE tbl_approve_work (
    approve_id          INT(6)   UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    create_time         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    book_id             INT(6)        NOT NULL,
    is_split_money      BOOLEAN       NOT NULL DEFAULT false
)
