<?php
    session_start();

    require_once('connection.php');

    if (!$_SESSION['userid']) {
        header("Location: login.php");
    } else{

   

?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
        
    </head>
    <style>
        .buttonsubmit, .buttonsubmit:visited, .buttonsubmit:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #9ea86b;
                padding: 15px 30px;
                font-size: 17px;
                width: 105px;
                height: 52px;
                border-radius: 12px;
                line-height: auto;              
            }
                .buttonsubmit:hover, .buttonsubmit:active {
                    background-color: #181A1C;
                    color: #efeae1;
                                    }

            .buttoncancle, .buttoncancle:visited, .buttoncancle:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #181A1C;
                padding: 15px 30px;
                font-size: 17px;
                width: 105px;
                height: 52px;
                border-radius: 12px;
                line-height: auto;              
            }

            .buttoncancle:hover, .buttoncancle:active {
                    background-color: #9ea86b;
                    color: #9ea86b;
                                    }
               

            .buttonmodal, .buttonmodal:visited, .buttonmodal:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #9ea86b;
                padding: 15px 30px;
                font-size: 17px;
                width: 92px;
                height: 42px;
                line-height: auto;   
                border-radius: 12px;
                padding-left: 22px; 
                padding-top: 10px;          
            }
                .buttonmodal:hover, .buttonmodal:active {
                    background-color: #181A1C;
                    color: #181A1C;
                                    }

                .buttonmodalcancle, .buttonmodalcancle:visited, .buttonmodalcancle:focus {
                display: inline-block;
                border: 1px solid #181A1C;
                color: #181A1C;
                background-color: #181A1C;
                padding: 15px 30px;
                font-size: 17px;
                width: 92px;
                height: 42px;
                line-height: auto;   
                border-radius: 12px;
                padding-left: 22px; 
                padding-top: 10px;          
            }
                .buttonmodalcancle:hover, .buttonmodalcancle:active {
                    background-color: #9ea86b;
                    color: #9ea86b;
                                    }


                select {
                    height: 40px;
                }
                .dss {
                            display: none;
                        }

                                    
                

        
    </style>
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

                        <img src="images/logo.png" width="180">
                        <!-- Logo -->

                    <!-- Button -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Menu -->
                    <div class="nav-collapse collapse pull-right">
                    <ul class="nav" id="top-navigation">
                            <li><a href="user.php">หน้าแรก</a></li>
                            <li><a href="user.php#service">บริการ</a></li>
                            <li><a href="user.php#barber">ช่างตัดผม</a></li>
                            <li><a href="user.php#hair">ทรงผม</a></li>
                            <li><a href="bookbarber.php">จองคิว</a></li>
                            <li  class="active"><a href="situation.php"><img src="images/bell.png"width="25"></a></li>
                            <li><a href=""><img src="images/user.png"width="25"> <?php echo $_SESSION['user']; ?></a></li>
                            <li><a href="logout.php"><img src="images/logout.png"width="25"> ออกจากระบบ</a></li>
                        </ul>
                    </div>
                    <!-- Menu -->
                </div>
            </div>
        </div>
        

		<!-- ชำระเงิน -->
        <div style="background-color: #efeae1;">
                <div class="triangle"></div>
                <div class="mask"></div>
            <div class="container">
                <div class="title">
                <center>
                    <h1><img src="images/book.png"></h1>
                    <h3><img src="images/pole.png"> แก้ไขข้อมูลการจองคิว <img src="images/pole.png"></h3>
                </center>
                </div><br>
                    <div class="row-fluid" style="padding-left: 300px;">
                        
						<div class="span6 price-column">
                                <form method="post" action="save_book_order.php" class="form-horizontal" enctype="multipart/form-data"><br>
                                    <br><br><br>
                                    <h4>คุณ : <input class="text-center" name="book_user" value="<?php echo $_SESSION['user']; ?>"></h4>
                                    <h4 style="margin-right: 68px;">ช่างให้บริการ : <input  class="text-center" name="book_barber" value="<?php echo "" . $_POST['book_barber'];?>"> </h4>
                                    <h4>วัน : <input  class="text-center" name="book_date" value="<?php echo "" . $_POST['book_date'];?>"> <h4>
                                    <h4 style="margin-right: 10px;">เวลา : <input  class="text-center" name="book_time" value="<?php echo "" . $_POST['book_time'];?>"> <h4>
                                    <h4 style="margin-right: 92px;">รายการให้บริการ : <input  class="text-center" name="book_service" value="<?php echo "" . $_POST['book_service'];?>"> <h4>
                                    <h4  style="margin-right: 25px;">ทรงผม : <input  class="text-center" name="book_hair" value="<?php echo "" . $_POST['book_hair'];?>"> <h4>
                                    <h4 style="margin-right: 35px;">ค่ามัดจำ : <input  class="text-center" name="book_pay" value="50"><br>
                                    <h4 style="margin-right: 13px;">สลิปการโอน : <a><input  class="text-center" type="file" name="uploadfile" required></a> <h4>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="centered service ">
                                                <div class=" zoom-in">
                                                <button type="submit"  class="buttonsubmit " style="color:white;">ยืนยัน</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" data-toggle="modal" data-target="#exampleModalCenter" class="buttoncancle" style="color:white;">ยกเลิก</button>
                                                
                                                
                                            
                                                </div>
                                            
                                            </div> 
                                            </div>       
                                        
                                    </div>
                                    
                                    <img src="images/line.png"><br><br>
                                  
                            </form>
                           <div style="margin-bottom: 95px;"></div>
                        </div>
                    </div>
            </div>
     </div>
  <!-- footer -->
  <!-- footer -->
  <div style="padding-top: 220px;background-color: #efeae1;"></div>
        <div class="section primary-section">
           
                <div class="container">
                    <center>
                   <h1>footer</h1>
                    </center>
                </div>
            </div>
    </div>
       <!-- modal -->
       <div class="modal fade dss" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                            <div class="modal-header  justify-content-center">
                                <img src="images/animation.gif" width="360px" style="padding-left: 80px;">
                                <h3 class="text-center"style="color:black;">warning!</h3>
                                <h5 class="text-center"style="color:black;">ข้อมูลของลูกค้าจะไม่ถูกบันทึกต้องการยกเลิกการแก้ไขหรือไม่</h5>
                            </div>
                        
                            
                            <div class="modal-footer">

                            <button onclick="location.href='situation.php'" type="button" class="buttonmodal"style="color:white;">ยกเลิก</button>
                                <button type="button" class="buttonmodalcancle" data-dismiss="modal"style="color:white;">ปิด</button>
                            </div>
                            </div>
                        </div>
        </div>
        <!-- javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
<?php } ?>