<?php
    session_start();
?>


<nav class="navbar navbar-expand-sm  fixed-top nav-cus-background ">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="./images/logo.png" alt=""width="150" class="d-inline-block align-text-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav d-flex flex-grow-1">
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="1"){ echo 'active';} ?>" href="admin.php">ตารางเวลา</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="2"){ echo 'active';} ?>" href="adminbookrequest.php">รายการจองคิว</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="3"){ echo 'active';} ?>" href="adminbookaccept.php">ยืนยันการชำระเงิน</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="4"){ echo 'active';} ?>" href="adminmoneymanagement.php">จัดการบัญชีร้าน</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="5"){ echo 'active';} ?>" href="adminuser.php">ข้อมูลลูกค้า</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="6"){ echo 'active';} ?>" href="adminservices.php">ข้อมูลบริการ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="7"){ echo 'active';} ?>" href="adminbarber.php">ข้อมูลช่าง</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if($_SESSION['page']=="8"){ echo 'active';} ?>" href="adminhair.php">ข้อมูลทรงผม</a>
        </li>
        
      </ul>
      <form class="d-flex ">
        <ul class="navbar-nav d-flex flex-grow-0">
          <li class="nav-item">
            <a class="nav-link" href="admin_mainV2.php">
              <img src="./images/user.png" width="25">
              ADMIN
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">
              <img src="./images/logout.png" width="25">
              ออกจากระบบ
            </a>
          </li>
        </ul>
      </form>
    </div>
  </div>
</nav>