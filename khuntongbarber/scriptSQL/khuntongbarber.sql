-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2022 at 02:34 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `khuntongbarber`
--

-- --------------------------------------------------------

--
-- Table structure for table `hair`
--

CREATE TABLE `hair` (
  `hair_id` int(20) NOT NULL,
  `hair_name` varchar(100) NOT NULL,
  `hair_image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hair`
--

INSERT INTO `hair` (`hair_id`, `hair_name`, `hair_image`) VALUES
(1, 'Pompadour', 'pompadour.png'),
(2, 'Skin Head', 'skinhead.png'),
(3, 'Hard Part', 'hardpart.png'),
(4, 'Flat Top', 'flattop.png'),
(5, 'Executive Contour', 'executive contour.png'),
(6, 'Under Cut', 'undercut.png'),
(59, 'รองทรงสูง', 'Screenshot (8).png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_approve_work`
--

CREATE TABLE `tbl_approve_work` (
  `approve_id` int(6) UNSIGNED NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `book_id` int(6) NOT NULL,
  `is_split_money` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_approve_work`
--

INSERT INTO `tbl_approve_work` (`approve_id`, `create_time`, `book_id`, `is_split_money`) VALUES
(1, '2022-04-21 15:03:58', 3, 0),
(4, '2022-04-22 08:49:10', 8, 0),
(5, '2022-04-22 08:49:16', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barber`
--

CREATE TABLE `tbl_barber` (
  `barber_id` int(6) UNSIGNED NOT NULL,
  `names` varchar(128) NOT NULL,
  `age` varchar(2) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(256) NOT NULL,
  `images` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_barber`
--

INSERT INTO `tbl_barber` (`barber_id`, `names`, `age`, `phone`, `email`, `images`) VALUES
(7, 'แพร', '15', '0996355125', 'dwqdqwdqwdqwdqwd', 'bbimage-1088256424.png'),
(10, 'พี่แพร', '15', '0955587878', 'pair@mail.com', 'bbimage-2102921489.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_book`
--

CREATE TABLE `tbl_book` (
  `book_id` int(6) UNSIGNED NOT NULL,
  `users_id` int(20) NOT NULL,
  `services_id` int(6) NOT NULL,
  `services_price` int(10) NOT NULL,
  `services_duration` int(10) NOT NULL,
  `time_id` int(6) NOT NULL,
  `book_time` varchar(20) NOT NULL,
  `images` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `book_status` varchar(20) NOT NULL DEFAULT 'WAIT_APPROVE',
  `deposit_price` int(10) NOT NULL DEFAULT 50
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_book`
--

INSERT INTO `tbl_book` (`book_id`, `users_id`, `services_id`, `services_price`, `services_duration`, `time_id`, `book_time`, `images`, `create_time`, `book_status`, `deposit_price`) VALUES
(3, 38, 2, 200, 50, 135, '11.00 น', 'bookimage-168602943.png', '2022-04-20 13:59:07', 'APPROVE', 50),
(4, 38, 2, 200, 50, 190, '13.00 น', 'bookimage-243947075.png', '2022-04-20 15:22:49', 'REJECT', 50),
(5, 38, 2, 200, 50, 194, '14.00 น', 'bookimage-1667256185.png', '2022-04-20 15:25:43', 'APPROVE', 50),
(6, 38, 3, 60, 30, 190, '13.00 น', 'bookimage-648143603.png', '2022-04-20 15:33:41', 'APPROVE', 50),
(7, 38, 2, 200, 50, 202, '14.00 น', 'bookimage-397580846.png', '2022-04-21 12:42:58', 'APPROVE', 50),
(8, 38, 5, 200, 60, 201, '13.00 น', 'bookimage-596426832.png', '2022-04-21 12:43:31', 'APPROVE', 50),
(9, 38, 3, 60, 30, 203, '11.00 น', 'bookimage-2093973822.png', '2022-05-10 12:28:15', 'APPROVE', 50);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hair`
--

CREATE TABLE `tbl_hair` (
  `hair_id` int(6) UNSIGNED NOT NULL,
  `names` varchar(128) NOT NULL,
  `images` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_hair`
--

INSERT INTO `tbl_hair` (`hair_id`, `names`, `images`) VALUES
(1, 'Pompadour', 'pompadour.png'),
(2, 'Skin Head', 'skinhead.png'),
(3, 'Hard Part', 'hardpart.png'),
(4, 'Flat Top', 'flattop.png'),
(5, 'Executive Contour', 'executive contour.png'),
(6, 'Under Cut', 'undercut.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE `tbl_services` (
  `services_id` int(6) UNSIGNED NOT NULL,
  `names` varchar(128) NOT NULL,
  `price` int(10) NOT NULL,
  `duration` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`services_id`, `names`, `price`, `duration`) VALUES
(2, 'ผู้ใหญ่', 200, 50),
(3, 'โกนหนวด', 60, 30),
(4, 'เซ็ทผม', 60, 10),
(5, 'ย้อมผม', 200, 60),
(6, 'เด็ก (ต่ำกว่า 18 ปี)', 50, 60);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_time`
--

CREATE TABLE `tbl_time` (
  `time_id` int(6) UNSIGNED NOT NULL,
  `date_time` datetime NOT NULL,
  `time_str` varchar(20) NOT NULL,
  `barber_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_time`
--

INSERT INTO `tbl_time` (`time_id`, `date_time`, `time_str`, `barber_id`) VALUES
(135, '2022-04-21 00:00:00', '11.00', 10),
(185, '2022-04-21 00:00:00', '13.00', 7),
(186, '2022-04-21 00:00:00', '14.00', 7),
(187, '2022-04-21 00:00:00', '15.00', 7),
(188, '2022-04-21 00:00:00', '16.00', 7),
(189, '2022-04-21 00:00:00', '17.00', 7),
(190, '2022-04-21 00:00:00', '13.00', 10),
(194, '2022-04-21 00:00:00', '14.00', 10),
(198, '2022-04-21 00:00:00', '15.00', 10),
(199, '2022-04-21 00:00:00', '17.00', 10),
(200, '2022-04-22 00:00:00', '11.00', 7),
(201, '2022-04-22 00:00:00', '13.00', 7),
(202, '2022-04-22 00:00:00', '14.00', 7),
(203, '2022-05-11 00:00:00', '11.00', 10),
(204, '2022-05-11 00:00:00', '13.00', 10),
(205, '2022-05-11 00:00:00', '14.00', 10),
(206, '2022-05-11 00:00:00', '15.00', 10),
(207, '2022-05-11 00:00:00', '16.00', 10),
(208, '2022-05-11 00:00:00', '17.00', 10),
(209, '2022-05-11 00:00:00', '18.00', 10),
(210, '2022-05-11 00:00:00', '19.00', 10),
(211, '2022-05-11 00:00:00', '20.00', 10),
(212, '2022-05-11 00:00:00', '21.00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `userlevel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `phone`, `userlevel`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', '0639415038', 'admin'),
(18, 'francksiriwut', 'fde94d06221d9ff13bf246cb3501a549', 'francksiriwut@gmail.com', '', 'member'),
(36, 'titi', '81dc9bdb52d04dc20036dbd8313ed055', 'titinan.ru@rmuti.ac.th', '', 'member'),
(37, 'pare', '81dc9bdb52d04dc20036dbd8313ed055', 'aon.apple558@gmail.com', '', 'member'),
(38, 'titinan', '81dc9bdb52d04dc20036dbd8313ed055', 'aon.craze58@gmail.com', '', 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hair`
--
ALTER TABLE `hair`
  ADD PRIMARY KEY (`hair_id`);

--
-- Indexes for table `tbl_approve_work`
--
ALTER TABLE `tbl_approve_work`
  ADD PRIMARY KEY (`approve_id`);

--
-- Indexes for table `tbl_barber`
--
ALTER TABLE `tbl_barber`
  ADD PRIMARY KEY (`barber_id`);

--
-- Indexes for table `tbl_book`
--
ALTER TABLE `tbl_book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `tbl_hair`
--
ALTER TABLE `tbl_hair`
  ADD PRIMARY KEY (`hair_id`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
  ADD PRIMARY KEY (`services_id`);

--
-- Indexes for table `tbl_time`
--
ALTER TABLE `tbl_time`
  ADD PRIMARY KEY (`time_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hair`
--
ALTER TABLE `hair`
  MODIFY `hair_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tbl_approve_work`
--
ALTER TABLE `tbl_approve_work`
  MODIFY `approve_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_barber`
--
ALTER TABLE `tbl_barber`
  MODIFY `barber_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_book`
--
ALTER TABLE `tbl_book`
  MODIFY `book_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_hair`
--
ALTER TABLE `tbl_hair`
  MODIFY `hair_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
  MODIFY `services_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_time`
--
ALTER TABLE `tbl_time`
  MODIFY `time_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
