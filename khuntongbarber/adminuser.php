<?php 
session_start();
$_SESSION['page'] = '5';

if (!$_SESSION['userid']) {
    header("Location: login.php");
} 
?>

<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- IMPORTSCRIPT -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
                integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
                crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <!-- END -->
        <link rel="shortcut icon" href="images/icon.png">
        
    </head>

    <body class="custom-body-color">
    
        <?php include 'navbarV2.php'; ?>
     
        <!-- ข้อมูลลูกค้า -->
        <div class="section primary-section custom-nav-margin" id="user">
            <div class="container">

            <div class="text-center mb-4 text-main">
                <h2 style="color:  #9ea86b;"><img src="images/pole.png"> ข้อมูลลูกค้า <img src="images/pole.png"></h2>
                <img src="images/line.png">
            </div>

                <div class="section primary-section">
                    <table class="w3-table table text-main">
                        <thead>
                            <tr class="w3-border">
                                <td><center>ชื่อผู้ใช้</center></td>
                                <td><center>อีเมล</center></td>
                                <td><center>สิทธิ์</center></td>
                                <td><center>ลบข้อมูล</center></td>
                            </tr>
                        </thead>

                        <tbody id="table_content">
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php include 'footer.php'; ?>
        <!-- END -->

    <!-- CUSTOMJS -->
    <script src="./vendor2/jquery/jquery.min.js"></script>
    <script src="./vendor2/jquery-easing/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./JsScript/adminuser.js"></script>
    <link rel="stylesheet" type="text/css" href="css/upload.css" />
    <link href="./customCss/user.css" rel="stylesheet">
    <!-- END -->
    </body>
    </body>
</html>