<?php

    session_start();
    $_SESSION['page'] = '1';
    require_once('connection.php');

    if (!$_SESSION['userid']) {
        header("Location: login.php");
    } 

    

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khuntong Barber</title>
        <!-- IMPORTSCRIPT -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
                integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
                crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <!-- END -->

        <!-- Icon -->
        <link rel="shortcut icon" href="images/icon.png">
    </head>

    <body class="custom-body-color">

        <?php include 'navbarV2.php'; ?>

    
        <!--  CONTENT -->
        <div class="container col-lg-12 custom-nav-margin">
            <!-- TITLE -->
            <div class="container p-4">
            <div class="text-center text-main">
                <h2>
                <img src="./images/pole.png">
                จัดการตารางเวลา
                <img src="./images/pole.png">
                </h2>
            </div>
            <div class="text-center">
                <img src="./images/line.png" >
            </div>
            </div>
            <!-- END -->

            <div class="card">
                <h3 class="card-header" id="monthAndYear"></h3>
                <table class="table table-bordered table-responsive-sm" id="calendar">
                    <thead>
                      <tr>
                          <th>Sun</th>
                          <th>Mon</th>
                          <th>Tue</th>
                          <th>Wed</th>
                          <th>Thu</th>
                          <th>Fri</th>
                          <th>Sat</th>
                      </tr>
                    </thead>

                    <tbody id="calendar-body">

                    </tbody>
                    
                </table>

                <div class="d-flex">
                    <button class="btn btn-outline-custom col-sm-6" id="previous" onclick="previous()">Previous</button>

                    <button class="btn btn-outline-custom col-sm-6" id="next" onclick="next()">Next</button>
                </div>
                <br/>
                <form class="form-inline d-none">
                    <label class="lead mr-2 ml-2" for="month">Jump To: </label>
                    <select class="form-control col-sm-4" name="month" id="month" onchange="jump()">
                        <option value=0>Jan</option>
                        <option value=1>Feb</option>
                        <option value=2>Mar</option>
                        <option value=3>Apr</option>
                        <option value=4>May</option>
                        <option value=5>Jun</option>
                        <option value=6>Jul</option>
                        <option value=7>Aug</option>
                        <option value=8>Sep</option>
                        <option value=9>Oct</option>
                        <option value=10>Nov</option>
                        <option value=11>Dec</option>
                    </select>


                    <label for="year"></label><select class="form-control col-sm-4" name="year" id="year" onchange="jump()">
                    <option value=1990>1990</option>
                    <option value=1991>1991</option>
                    <option value=1992>1992</option>
                    <option value=1993>1993</option>
                    <option value=1994>1994</option>
                    <option value=1995>1995</option>
                    <option value=1996>1996</option>
                    <option value=1997>1997</option>
                    <option value=1998>1998</option>
                    <option value=1999>1999</option>
                    <option value=2000>2000</option>
                    <option value=2001>2001</option>
                    <option value=2002>2002</option>
                    <option value=2003>2003</option>
                    <option value=2004>2004</option>
                    <option value=2005>2005</option>
                    <option value=2006>2006</option>
                    <option value=2007>2007</option>
                    <option value=2008>2008</option>
                    <option value=2009>2009</option>
                    <option value=2010>2010</option>
                    <option value=2011>2011</option>
                    <option value=2012>2012</option>
                    <option value=2013>2013</option>
                    <option value=2014>2014</option>
                    <option value=2015>2015</option>
                    <option value=2016>2016</option>
                    <option value=2017>2017</option>
                    <option value=2018>2018</option>
                    <option value=2019>2019</option>
                    <option value=2020>2020</option>
                    <option value=2021>2021</option>
                    <option value=2022>2022</option>
                    <option value=2023>2023</option>
                    <option value=2024>2024</option>
                    <option value=2025>2025</option>
                    <option value=2026>2026</option>
                    <option value=2027>2027</option>
                    <option value=2028>2028</option>
                    <option value=2029>2029</option>
                    <option value=2030>2030</option>
                  </select>
                </form>
            </div>
        </div>
        <!--  ENDCONTENT -->



        <?php include 'footer.php'; ?>
      
        <!-- footer -->
     
<!-- MODAL -->
<div class="modal fade" id="modal_mng_times" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">จัดการเวลา</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-6">
                  <label class="fw-bolder">วันที่ : <span id="txt_mng_date"> </span></label>
                 
              </div>
              <div class="col-6">
                  <label class="fw-bolder">ช่าง :</label>
                  <select class="form-select" id="sel_barber">
                  </select>
              </div>
          </div>
          <div class="row">
              <div class="col-6">
                  <label class="fw-bolder">รออนุมัติ : <span class="badge bg-warning"> </span></label>
                  <label class="fw-bolder">อนุมัติ : <span class="badge bg-success"> </span></label>
              </div>
          </div>
          <div class="row mt-2">
              <div class="col-12">
                 <table class="table table-bordered table-responsive-sm">
                     <tr>
                        <td class="" id="check_11_block">
                         <div class="form-check">
                           <input class="form-check-input" type="checkbox" value="11.00" id="check_11">
                           <label class="form-check-label" for="check_11">
                             11.00 น
                           </label>
                         </div>
                        </td>
                        <td class="" id="check_13_block">
                         <div class="form-check">
                           <input class="form-check-input" type="checkbox" value="13.00" id="check_13">
                           <label class="form-check-label" for="check_13">
                             13.00 น
                           </label>
                         </div>
                        </td>
                        <td class="" id="check_14_block">
                         <div class="form-check">
                           <input class="form-check-input" type="checkbox" value="14.00" id="check_14">
                           <label class="form-check-label" for="check_14">
                             14.00 น
                           </label>
                         </div>
                        </td>
                        <td class="" id="check_15_block">
                         <div class="form-check">
                           <input class="form-check-input" type="checkbox" value="15.00" id="check_15">
                           <label class="form-check-label" for="check_15">
                             15.00 น
                           </label>
                         </div>
                        </td>
                        <td class="" id="check_16_block">
                         <div class="form-check">
                           <input class="form-check-input" type="checkbox" value="16.00" id="check_16">
                           <label class="form-check-label" for="check_16">
                             16.00 น
                           </label>
                         </div>
                        </td>
                        <td class="" id="check_17_block">
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="17.00" id="check_17">
                            <label class="form-check-label" for="check_17">
                              17.00 น
                            </label>
                          </div>
                        </td>
                        <td class="" id="check_18_block">
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="18.00" id="check_18">
                            <label class="form-check-label" for="check_18">
                              18.00 น
                            </label>
                          </div>
                        </td>
                        <td class="" id="check_19_block">
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="19.00" id="check_19">
                            <label class="form-check-label" for="check_19">
                              19.00 น
                            </label>
                          </div>
                        </td>
                     </tr>
                     <tr>
                      <td class="" id="check_20_block">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="20.00" id="check_20">
                          <label class="form-check-label" for="check_20">
                            20.00 น
                          </label>
                        </div>
                      </td>
                      <td class="" id="check_21_block">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="21.00" id="check_21">
                          <label class="form-check-label" for="check_21">
                            21.00 น
                          </label>
                        </div>
                      </td>
                  </tr>
                 </table>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="btn_save_time"  data-bs-dismiss="modal">ยืนยัน</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
      </div>
    </div>
  </div>
</div>
<!-- ENDMODAL -->
    </body>
    <!-- CUSTOM -->
    <link href="./customCss/admintimes.css" rel="stylesheet">
    <script src="./JsScript/middleware.js"></script>
    <script src="./JsScript/admintimes.js"></script>
    <!-- END -->
    </html>

